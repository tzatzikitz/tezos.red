const { Pool } = require('pg')

var pool

function uniq(array, into = [], memo = new Set()) {
  for (let item of array) {
    if (!memo.has(item)) {
      memo.add(item)
      into.push(item)
    }
  }
  return into
}

function whereClause(filters) {
  let counter = 1
  const none = { fs: [], args: [] }
  function zoomFilters(zoom) {
    return {
      fs: [
        `s.x >= $${counter++}`,
        `s.x <= $${counter++}`,
        `s.y >= $${counter++}`,
        `s.y <= $${counter++}`,
      ],
      args: [zoom.xs, zoom.xe, zoom.ys, zoom.ye]
    }
  }

  function managerFilter(mng) {
    return {
      fs: [`c.manager = $${counter++}`],
      args: [mng]
    }
  }

  function typeFilter(type) {
    if (type === 'smart') {
      return {
        fs: ['c.codesize > 0'],
        args: []
      }
    }
    if (type === 'dumb') {
      return {
        fs: ['c.codesize IS NULL'],
        args: []
      }
    }
    return none
  }

  function showManagersFilter(sm) {
    return !sm ? {
      fs: ['c.id <> c.manager'],
      args: []
    } : none
  }

  let fs = []
  let args = []
  const zoom = filters.zoom ? zoomFilters(filters.zoom) : none
  const manager = filters.manager ? managerFilter(filters.manager) : none
  const type = typeFilter(filters.type)
  const smf = showManagersFilter(filters.showManagers)
  fs = fs.concat(zoom.fs)
    .concat(manager.fs)
    .concat(type.fs)
    .concat(smf.fs)
  args = args.concat(zoom.args)
    .concat(manager.args)
    .concat(type.args)

  if (fs.length === 0) return [ '', args ]
  if (fs.length === 1) return [ 'WHERE ' + fs[0], args ]
  return [ 'WHERE ' + fs.join(' AND '), args ]
}

function fixContract(x) {
  if(!x) return
  const result = Object.assign({}, x, x.details)
  result.balance = Number(x.balance)
  result.manager = {
    id: result.manager,
    ownBalance: Number(result.managerownbalance),
    totalBalance: Number(result.managertotalbalance)
  }
  result.delegate = {
    name: result.delegatename,
    address: result.delegateaddress
  }
  result.volume = {
    1: Number(result.volume1),
    7: Number(result.volume7),
    30: Number(result.volume30)
  }
  delete result.volume1
  delete result.volume7
  delete result.volume30
  delete result.managerownbalance
  delete result.managertotalbalance
  delete result.delegatename
  delete result.delegateaddress
  return result
}

function fixDelegate(x) {
  let result = Object.assign({}, x, {
    stake: Number(x.stake),
    clients: Number(x.clients),
    accounts: Number(x.accounts)
  })
  return result
}

function wrapResult(x) {
  return {
    status: 'OK',
    result: x
  }
}

async function graph(core) {
  function nc(xs) {
    return xs.reduce((acc, x) => acc.concat(x), [])
  }
  let times = [], t0, t1
  t0 = process.hrtime()
  const managers = (await pool.query(`SELECT id, manager FROM contracts c
WHERE (id = ANY ($1) OR manager = ANY ($1)) AND manager IS NOT NULL`, [core])).rows.map(x => [x.id, x.manager])
  t1 = process.hrtime(t0)
  times.push([ t1[0], t1[1] / 1000000 ])

  t0 = process.hrtime()
  const delegates = (await pool.query(`SELECT id, delegate, d.name FROM contracts c
LEFT OUTER JOIN delegates d ON d.address = c.delegate
WHERE (id = ANY ($1) OR delegate = ANY ($1)) AND delegate IS NOT NULL
`, [core])).rows.map(x => [x.id, x.delegate, x.name])
  t1 = process.hrtime(t0)
  times.push([ t1[0], t1[1] / 1000000 ])

  t0 = process.hrtime()
  const transactions = (await pool.query(`
SELECT DISTINCT src, dst FROM transactions t
WHERE kind = 'transaction'
AND src = ANY ($1) OR dst = ANY ($1)`, [core])).rows
  t1 = process.hrtime(t0)
  times.push([ t1[0], t1[1] / 1000000 ])

  let nextcore = uniq(nc(managers)
    .concat(nc(delegates))
    .concat(nc(transactions.map(({ src, dst }) => [src, dst]))))

  t0 = process.hrtime()
  const details = (await pool.query(`
SELECT c.id, c.balance, m.totalbalance FROM contracts c
LEFT OUTER JOIN managers m ON c.id = m.id
WHERE c.id = ANY ($1)`, [nextcore])).rows.map(r => {
    return {
      id: r.id,
      balance: Number(r.balance),
      totalBalance: Number(r.totalbalance)
    }
  })
  t1 = process.hrtime(t0)
  times.push([ t1[0], t1[1] / 1000000 ])

  // TODO originated contracts
  // TODO label genesis contracts
  return {
    status: 'OK',
    delegates,
    details,
    times,
    managers: managers,
    core,
    nextcore,
    transactions
  }
}

module.exports = {
  connect: async () => {
    pool = new Pool({
      database: 'tezos-red'
    })
    await pool.connect()
    return pool
  },
  end: async () => { await pool.end() },
  contracts: {
    get: async (id) => {
      const result = fixContract((await pool.query(`SELECT c.id, c.manager,
c.activated, c.originated, c.balance, c.delegate AS delegateaddress,
c.counter, c.codesize, c.annotations, c.primitives, d.name AS delegatename,
c.script, c.json AS details,
COALESCE(v.volume1, 0) as volume1,
COALESCE(v.volume7, 0) as volume7,
COALESCE(v.volume30, 0) as volume30,
COALESCE(m.ownbalance, 0) as managerownbalance,
COALESCE(m.totalbalance, 0) as managertotalbalance
FROM contracts c
LEFT OUTER JOIN volume v ON c.id = v.contract
LEFT OUTER JOIN managers m ON m.id = c.manager
LEFT OUTER JOIN delegates d ON c.delegate = d.address
WHERE c.id = $1`, [id])).rows[0])
      if (!result) return { status: 'NOTFOUND' }
      const manager = result.manager.id
      result.manager.contracts = (await pool.query('SELECT id, balance FROM contracts WHERE manager = $1 AND id <> $1', [manager])).rows.map(c => {
        return Object.assign({}, c, {
          balance: Number(c.balance)
        })
      })
      return wrapResult(result)
    },
    all: async ({ limit, orderby, orderdir, offset, filter }) => {
      const total = Number((await pool.query('SELECT count(*) FROM contracts')).rows[0].count)
      const [ where, args ] = whereClause(filter)
      const filtered = Number((await pool.query(`
SELECT count(*) FROM contracts c
LEFT OUTER JOIN similarity s ON s.contract = c.id
${where}`, args)).rows[0].count)
      const result = (await pool.query(`
SELECT c.id, c.manager, c.activated, c.originated, c.balance, c.delegate AS
delegateaddress, d.name AS delegatename, c.counter, c.codesize, c.spendable,
c.annotations, c.primitives, s.x, s.y, p.sum_addresses AS promiscuity,
COALESCE(v.volume1, 0) as volume1,
COALESCE(v.volume7, 0) as volume7,
COALESCE(v.volume30, 0) as volume30,
COALESCE(m.ownbalance, 0) as managerownbalance,
c.json #- '{script}' AS details
FROM contracts c
LEFT OUTER JOIN volume v ON c.id = v.contract
LEFT OUTER JOIN managers m ON m.id = c.id
LEFT OUTER JOIN similarity s ON s.contract = c.id
LEFT OUTER JOIN delegates d ON c.delegate = d.address
LEFT OUTER JOIN promiscuity p ON p.id = c.id
${where}
ORDER BY ${orderby} ${orderdir} NULLS LAST LIMIT $${args.length + 1} OFFSET ${offset}`, [
        ...args, limit
      ]))
      return {
        items: result.rows.map(fixContract),
        total,
        filtered,
        pages: Math.ceil(filtered / limit),
        status: 'OK'
      }
    }
  },
  managers: {
    all: async ({ limit, orderby, orderdir, offset }) => {
      const total = Number((await pool.query('SELECT count(*) FROM managers')).rows[0].count)
      const result = (await pool.query(`
SELECT m.*, p.sum_addresses AS promiscuity
FROM managers m
LEFT OUTER JOIN promiscuity p ON p.id = m.id
ORDER BY ${orderby} ${orderdir} NULLS LAST LIMIT $1 OFFSET ${offset}`, [limit]))
      return {
        items: result.rows.map(result => {
          result.volume = {
            1: Number(result.volume1),
            7: Number(result.volume7),
            30: Number(result.volume30)
          }
          result.ownBalance = Number(result.ownbalance),
          result.totalBalance = Number(result.totalbalance)
          delete result.ownbalance,
          delete result.totalbalance,
          delete result.volume1
          delete result.volume7
          delete result.volume30
          return result
        }),
        total,
        filtered: total, // TODO: add filtered count
        pages: Math.ceil(total / limit),
        status: 'OK'
      }
    }
  },
  similarity: {
    all: async() => {
      const result = (await pool.query(`
SELECT id, codesize, x, y
FROM contracts c
LEFT OUTER JOIN similarity s ON s.contract = c.id
WHERE codesize <> 0`))
      return result.rows
        .reduce((acc, x) => {
          const y = x['id']
          acc[y] = x
          return acc
        }, {})
    }
  },
  meta: async () => {
    const result = (await pool.query(
      'SELECT ts from blocks ORDER BY ts DESC LIMIT 1'))
    const lastBlock = result.rows[0].ts
    return {
      status: 'OK',
      lastBlock
    }
  },
  delegates: {
    all: async ({ limit, offset, orderby, orderdir }) => {
      const total = Number((await pool.query('SELECT count(*) FROM delegates WHERE stake > 0')).rows[0].count)
      const result = (await pool.query(`
SELECT cd.delegate AS address, d.name, d.stake, cd.clients, cd.accounts, cd.ratio
FROM delegate_clients cd
LEFT OUTER JOIN delegates d ON cd.delegate = d.address
WHERE stake > 0
ORDER BY ${orderby} ${orderdir} NULLS LAST LIMIT ${limit} OFFSET ${offset}`))
      return {
        items: result.rows.map(fixDelegate),
        total,
        filtered: total,
        pages: Math.ceil(total / limit),
        status: 'OK'
      }
    }
  },
  graph: {
    get: async (core=[]) => {
      return await graph(core)
    }
  },
  transactions: {
    all: async ({ limit, offset, orderby, orderdir, params }) => {
      const total = Number((await pool.query(
        'SELECT count(*) from transactions WHERE kind = \'transaction\' AND (src = $1 OR dst = $1)', [params.id])).rows[0].count)
      const result = (await pool.query(`
SELECT src, dst, amount, ts from transactions
WHERE kind = 'transaction' AND (src = $1 OR dst = $1)
ORDER BY ${orderby} ${orderdir} NULLS LAST LIMIT ${limit} OFFSET ${offset}`, [params.id]))
      return {
        items: result.rows,
        total,
        filtered: total,
        pages: Math.ceil(total / limit),
        status: 'OK'
      }
    }
  }
}
