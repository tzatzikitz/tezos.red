const db = require('../api/db')
const createCsvWriter = require('csv-writer').createObjectCsvWriter
const csvWriterManagers = createCsvWriter({
  path: './managers.csv',
  header: [
    { id: 'source', title: 'SOURCE' },
    { id: 'target', title: 'TARGET' }
  ]
})
const csvWriterDelegates = createCsvWriter({
  path: './delegates.csv',
  header: [
    { id: 'source', title: 'SOURCE' },
    { id: 'target', title: 'TARGET' }
  ]
})
const csvWriterTransactions = createCsvWriter({
  path: './transactions.csv',
  header: [
    { id: 'source', title: 'SOURCE' },
    { id: 'target', title: 'TARGET' }
  ]
})

async function main() {
  const ms = []
  const ds = []
  const ts = []

  await db.connect()
  const { managers, delegates, transactions } = await db.graph.get()
  for (let [contract, manager] of managers) {
    ms.push({ source: contract, target: manager })
  }
  for (let [contract, delegate] of delegates) {
    ds.push({ source: contract, target: delegate })
  }
  for (let { src, dst } of transactions) {
    ts.push({ source: src, target: dst })
  }

  await csvWriterManagers.writeRecords(ms)
  await csvWriterDelegates.writeRecords(ds)
  await csvWriterTransactions.writeRecords(ts)
  console.log('done')
}

if (require.main === module) {
  main()
}

module.exports = main
