#!/usr/bin/env node

var express = require('express')
var app = express()

const db = require('./db')
db.connect()

function badParams(res, param, value) {
  res.json({
    status: 'ERROR',
    message: `Query parameter '${param}' can not be '${value}'`
  })
}

app.use(function(req, res, next) {
  var allowedOrigins = ['http://localhost:3000', 'https://tezos.red']
  var origin = req.headers.origin
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Origin', origin)
  }
  res.header('Access-Control-Allow-Methods', 'GET')
  return next()
})

app.get('/', function(req, res) {
  res.send('Welcome to the Tezos.red API!')
})

function getFilter(query) {
  let result = {
    type: query['filter.type'] || 'all',
    showManagers: query['filter.showManagers'] === 'true',
    manager: query['filter.manager'],
    zoom: {
      xs: Number(query['filter.zoom.xs']),
      xe: Number(query['filter.zoom.xe']),
      ys: Number(query['filter.zoom.ys']),
      ye: Number(query['filter.zoom.ye'])
    }
  }
  if (Object.values(result.zoom).some(x => isNaN(x))) {
    delete result.zoom
  }
  if (typeof result.manager === 'undefined') {
    delete result.manager
  }
  return result
}

function paginatedView({ defaultSorting, allowedSorting, dbTable, params }) {
  return async function(req, res) {
    const PAGESIZE = { DEFAULT: 20, MAX: 100 }
    const limit = Math.max(
      0,
      Math.min(
        req.query.limit || PAGESIZE.DEFAULT,
        PAGESIZE.MAX))

    const orderby = req.query.orderby || defaultSorting
    const orderdir = req.query.orderdir || 'desc'

    if (!allowedSorting.includes(orderby)) {
      badParams(res, 'orderby', orderby)
    }
    if (!['asc', 'desc'].includes(orderdir)) {
      badParams(res, 'orderdir', orderdir)
    }

    const page = req.query.page || 1
    const offset = Math.max(0, limit * (page))
    const options = {
      orderby: orderby.toLowerCase().replace('.id', '').replace('.', ''),
      orderdir,
      limit,
      offset,
      params: req.params,
      filter: getFilter(req.query)
    }
    return res.json(await db[dbTable].all(options))
  }
}

app.get('/contracts', paginatedView({
  defaultSorting: 'volume1',
  allowedSorting: [
    'annotations',
    'balance',
    'delegate',
    'codesize',
    'id',
    'manager.ownBalance',
    'manager.id',
    'originated',
    'primitives',
    'spendable',
    'promiscuity',
    'volume.1',
    'volume.30',
    'volume.7',
  ],
  dbTable: 'contracts'
}))

app.get('/managers', paginatedView({
  defaultSorting: 'totalBalance',
  allowedSorting: [
    'activated',
    'contracts',
    'id',
    'ownBalance',
    'totalBalance',
    'promiscuity',
    'volume.1',
    'volume.30',
    'volume.7',
  ],
  dbTable: 'managers'
}))

app.get('/delegates', paginatedView({
  defaultSorting: 'stake',
  allowedSorting: [
    'accounts',
    'address',
    'clients',
    'name',
    'ratio',
    'stake',
  ],
  dbTable: 'delegates'
}))

app.get('/transactions/:id', paginatedView({
  defaultSorting: 'ts',
  allowedSorting: [
    'src',
    'ts',
    'dst',
    'amount'
  ],
  dbTable: 'transactions'
}))

app.get('/similarity', async function(req, res) {
  res.json(await db.similarity.all())
})

app.get('/contract/:id', async function(req, res) {
  res.json(await db.contracts.get(req.params.id))
})

app.get('/meta', async function(req, res) {
  res.json(await db.meta())
})

app.get('/graph', async function(req, res) {
  const { core } = req.query
  res.json(await db.graph.get(core && core.split(',')))
})

app.listen(3030)
