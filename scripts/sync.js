async function main() {
  console.time('main')
  await require('./contracts').main()
  await require('./tsne')()
  await require('./walk')({ stopOnSeenBlock: true })
  await require('./delegates')()
  console.timeEnd('main')
}

main()
