'use strict'

const sha256 = require('js-sha256')
const TSNE = require('tsne-js')
const { entries, keys } = Object
const { size: codeSize } = require('../src/common/size')
const { getSource } = require('./utils')
const db = require('../src/common/db')

const objectsMap = {} // mapping hash -> object
const contractsMap = {} // mapping hash -> [contracts]
const contractSource = {} // mapping contract -> source (write to file)

function deepHash(object, contract) {
  const { args } = object
  let result = {
    hash: sha256(JSON.stringify(object))
  }
  if (Array.isArray(object)) {
    object.map(x => deepHash(x, contract))
  }

  objectsMap[result.hash] = object
  if (!contractsMap[result.hash]) {
    contractsMap[result.hash] = []
  }
  if (!contractsMap[result.hash].includes(contract)) {
    contractsMap[result.hash].push(contract)
  }

  if (args && args.length) {
    result.args = args.map(a => deepHash(a, contract))
  }
  return result
}

async function main() {
  console.log('Calculating TSNE...')
  await db.connect()
  const contracts = await db.contract.all()
  const hashed = contracts
    .map(getSource)
    .map(({ contract, code }) => {
      if (code) {
        contractSource[contract] = code
        return {
          contract,
          hashes: code.map((o) => deepHash(o, contract))
        }
      }
    })
    .filter(x => x && x.hashes)

  console.log(`${hashed.length} contracts have code`)

  // chart is based on similarities, so we don't graph consider the parts that
  // are unique to this one contract
  const mapping = new Map(entries(contractsMap).filter(([, v]) => v.length > 1))
  console.log(mapping.size + ' hashes found')

  const rows = keys(contractSource)
  const columns = Array.from(mapping.keys())
  const result = []
  for (let i in rows) {
    const contract = rows[i]
    const vals = []
    for (let j = 0; j < mapping.size; j++) {
      const hash = columns[j]
      const contracts = mapping.get(hash)
      let v = (contracts && contracts.includes(contract))// ? 1 : 0
      vals.push(v)
    }
    result.push(vals)
  }

  let model = new TSNE({
    dim: 2,
    perplexity: 30.0,
    earlyExaggeration: 4.0,
    learningRate: 100.0,
    nIter: 300,
    metric: 'dice'
  })

  model.init({
    data: result,
    type: 'dense'
  })

  model.run()
  let outputScaled = model.getOutputScaled()

  const tsne = []
  for(let i in outputScaled) {
    const contract = keys(contractSource)[i]
    const [x, y] = outputScaled[i]
    const src = contractSource[contract]
    const size = codeSize(src)
    tsne.push({ contract, x, y, size })
  }
  await db.similarity.writeAll(tsne)
  await db.end()
}

module.exports = main
if (require.main === module) {
  main()
}
