const { rpc, getDetails } = require('./utils')
const Promise = require('bluebird')
const { walk, size } = require('../src/common/size')
const db = require('../src/common/db')


function extractPrimitives(details) {
  // extract primitives from storage
  if (details.script) {
    details.primitives = { storage: [] }
    walk([details.script.storage], obj => details.primitives.storage.push(obj))
    if(!details.primitives.storage.length) delete details.primitives
  }
  return details
}

function extractAnnotations(details) {
  // extract annotations
  if (details.script) {
    details.annotations = []
    details.script.code.map(
      arr => walk([arr], obj => {
        if (obj.annots) details.annotations = details.annotations.concat(obj.annots)
      }))
    if(!details.annotations.length) delete details.annotations
  }
  return details
}

async function queryDetails(contracts) {
  return await Promise.map(contracts, async (contract) => {
    let details
    try {
      details = await getDetails(contract)
    } catch (e) {
      console.error(e)
      process.exit(1)
    }
    details.balance = details.balance
    details.contract = contract

    details = extractPrimitives(details)
    details = extractAnnotations(details)
    if (details.script) details.codeSize = size(details.script.code)

    return details
  }, { concurrency: 32 })
}

async function main() {
  await db.connect()
  const contracts = await rpc('/chains/main/blocks/head/context/contracts')
  console.info(`${contracts.length} contracts in context of head`)
  console.log((await db.contract.clearOthers(contracts)).rowCount + ' contracts deleted')
  const details = await queryDetails(contracts)
  await Promise.map(details, d => db.contract.write(d))
  await db.end()
}

if (require.main === module) {
  main()
}

module.exports = {
  main,
  queryDetails
}
