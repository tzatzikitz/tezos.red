/**
 * Runs with cron minutely on local dev machine. Shows visual notification if
 * production has not been updated in a while (based on meta.json)
 */

const fetch = require('cross-fetch')
const notifier = require('node-notifier')

const threshold = 7 // minutes

function error(message) {
  notifier.notify({
    title: 'Error',
    message
  });
}

function trigger(minutesAgo) {
  const message = `Last update was ${minutesAgo} minutes ago. (Longer than ${threshold})`
  notifier.notify({
    title: 'No tezos.red updates for a while...',
    message
  });
}

async function request() {
  try {
    const data = await fetch('https://api.tezos.red/meta')
    const json = await data.json()
    return json
  } catch (e) {
    error('Could not fetch or parse tezos.red/meta.json')
  }
}

async function main() {
  const json = await request()
  const last = new Date(json.lastUpdated)
  const minutesAgo = (new Date() - last) / (60 * 1000)
  if (minutesAgo > threshold) {
    trigger(minutesAgo)
  }
}

main()
