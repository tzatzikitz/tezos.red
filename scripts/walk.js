const { rpc } = require('./utils')
const db = require('../src/common/db')

const operations = [
  require('./walk/contracts.js')
]

var level
var stopOnSeenBlock

async function getBlock(hash) {
  // query the database
  let cached = true
  let block = await db.block.get(hash)
  if (!block) {
    cached = false
    // if no record, query rpc
    try {
      block = await rpc(`/chains/main/blocks/${hash}`)
    } catch (e) {
      console.error('Could not query RPC. Is tezos-node running?', e)
    }
    await db.block.write(block)
  }
  return [ block, cached ]
}

async function walkBlocks(hash) {
  const [ block, cached ] = await getBlock(hash)

  const { predecessor } = block.header
  level = block.header.level
  operations.map(o => o.onBlock(block, cached))
  if (cached && stopOnSeenBlock) {
    // stop early
    console.log(`Stop on seen block: ${level}`)
    return
  } else if(predecessor !== hash) {
    // recursively walk the chain
    return walkBlocks(predecessor)
  } else {
    // reached Genesis
  }
}

async function main(options) {
  stopOnSeenBlock = options.stopOnSeenBlock
  const start = process.argv.length === 3 ? process.argv[2] : 'head'
  console.log(`Walking chain from block '${start}'`)

  const client = await db.connect()
  await client.query('BEGIN')
  try {
    operations.map(o => o.onStart(start))
    let ints = []
    ints.push(setInterval(() => {
      console.log(`Walking block #${level}`)
    }, 5000))
    ints.push(setInterval(async () => {
      await client.query('COMMIT')
      await client.query('BEGIN')
      console.log('Committed to db')
    }, 60000))
    await walkBlocks(start)

    ints.map(i => clearInterval(i))
    await client.query('REFRESH MATERIALIZED VIEW volume')
    await client.query('REFRESH MATERIALIZED VIEW managers')
    await client.query('REFRESH MATERIALIZED VIEW promiscuity')
    await client.query('REINDEX TABLE transactions')
    await client.query('COMMIT')
  } catch (e) {
    await client.query('ROLLBACK')
    console.error('ROLLBACK on walk', e)
  }
  await client.end()
}

if (require.main === module) {
  main({ stopOnSeenBlock: true })
}

module.exports = main
