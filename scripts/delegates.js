const db = require('../src/common/db')
const fetch = require('cross-fetch')
const Promise = require('bluebird')
const { rpc } = require('./utils')

function insert (d) {
  if (!d.address || !d.address.startsWith('tz')) return
  return db.delegate.insert(d)
}

const foundation = [
  { address: 'tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9', name: 'Foundation Baker 1' },
  { address: 'tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5', name: 'Foundation Baker 2' },
  { address: 'tz3bTdwZinP8U1JmSweNzVKhmwafqWmFWRfk', name: 'Foundation Baker 3' },
  { address: 'tz3NExpXn9aPNZPorRE4SdjJ2RGrfbJgMAaV', name: 'Foundation Baker 4' },
  { address: 'tz3UoffC7FG7zfpmvmjUmUeAaHvzdcUvAj6r', name: 'Foundation Baker 5' },
  { address: 'tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K', name: 'Foundation Baker 6' },
  { address: 'tz3VEZ4k6a4Wx42iyev6i2aVAptTRLEAivNN', name: 'Foundation Baker 7' },
  { address: 'tz3RB4aoyjov4KEVRbuhvQ1CKJgBJMWhaeB8', name: 'Foundation Baker 8' }
]

async function rpcDelegates() {
  return Promise.map(
    await rpc('/chains/main/blocks/head/context/delegates?active'),
    async d => {
      const details = await rpc(`/chains/main/blocks/head/context/delegates/${d}`)
      const { staking_balance } = details
      return { address: d, stake: Number(staking_balance) }
    }, { concurrency: 32 })
}

async function main() {
  const delegatesBalance = await rpcDelegates()
  await db.connect()
  const delegates =
    await (await fetch('http://tzscan.io/services.json')).json()
  await Promise.map(delegates, insert)
  await Promise.map(foundation, insert)
  await Promise.map(delegatesBalance, insert)
  await db.end()
}

if (require.main === module) {
  main()
}

module.exports = main
