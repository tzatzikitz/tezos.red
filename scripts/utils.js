const fetch = require('cross-fetch')
const { getCode } = require('../src/common/utils')

const DATA_DIR = './public/data'
const PORT = 8732
const PREFIX = `http://localhost:${PORT}`

// prevent EADDRNOTAVAIL after many requests
const agent = new require('http').Agent({ keepAlive:true })

async function rpc(path) {
  return (await fetch(`${PREFIX}${path}`, {
    headers: { 'Content-Type': 'application/json' },
    agent
  })).json()
}

function getDetails(contract) {
  const url = `/chains/main/blocks/head/context/contracts/${contract}`
  return rpc(url)
}

function getSource(details) {
  const { id, script } = details
  var code
  if (script && script.code) {
    code = getCode(script)
  }
  return { contract: id, code }
}

module.exports = {
  rpc,
  DATA_DIR,
  getDetails,
  getSource
}
