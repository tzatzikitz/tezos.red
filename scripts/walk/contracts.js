const db = require('../../src/common/db')

module.exports = {
  description: 'Contract origination and Account activation date',

  onStart: () => {},

  onBlock: async (block, cached) => {
    const timestamp = new Date(Date.parse(block.header.timestamp))

    try {
      block.operations.map(op => {
        op.map(o => {
          if (!o.contents) return
          o.contents.map((cnt, idx) => {
            const kind = cnt.kind || ''
            // write operation into transactions table
            !cached && db.transaction.add(o.hash, idx, timestamp, cnt)
            if (kind.startsWith('activ')) {
              const contract = cnt.metadata.balance_updates[0].contract
              db.contract.activatedOn(contract, timestamp)
            }
            if (kind.startsWith('origin')) {
              const contracts = cnt.metadata.operation_result.originated_contracts
              contracts && contracts.map(c => {
                db.contract.originatedOn(c, timestamp)
              })
            }
          })
        })
      })
    } catch (e) {
      console.error(`Error processing block ${block.hash}`, e)
    }
  }
}
