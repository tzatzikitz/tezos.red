#!env node
/**
 * This script is run by cron every minute.
 * It will update the database with the details of the specific contracts, that need to be as up-to-date as possible.
 */
const Promise = require('bluebird')
const db = require('../src/common/db')
const { queryDetails } = require('./contracts')

const contracts = [
  'KT1DLw2U9kWVhvRjBo6yjmgvj4gn8WWRbaVL', // world map contract
]

async function main() {
  await db.connect()
  const details = await queryDetails(contracts)
  await Promise.map(details, d => db.contract.write(d))
  await db.end()
}

main()
