# Tezos.red

Tezos Contract Explorer.

## Backend - Betanet

Ensure you have a node running and can get a json response from the RPC:

    curl http://127.0.0.1:8732/chains/main/blocks/head | jq '.header | .timestamp'

Run `npm run sync`. This will query the RPC and write the output to `public/data/`.

## Backend - Alphanet/Zeronet

Forward rpc port from docker container to host when starting the container:

    zeronet.sh start --rpc-port 7777

Ensure you can get a json response from the RPC:

    curl -X POST http://localhost:7777/blocks/head -d '{}'

Run `npm run sync`. This will query the RPC and write the output to `public/data/`.

## Frontend

Start the development server

    npm start
    
This will watch for changes in the JS and SCSS files and hot-reload them into the browser.

## Build and Deploy

    npm run build
