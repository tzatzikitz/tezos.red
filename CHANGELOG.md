# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [0.2.0] - 2018-07-31
### Added
- Graph feature for exploring relationships between contracts.
- Collection feature: group contracts together for later analysis with the Graph.

## [0.1.0] - 2018-07-25
### Added
- Delegate Table
- Promiscuity column, see with how many other contracts a contract has transacted.
- Volume columns, in the last 24h, 7 days and 30 days.
- Filter contracts by zoom
- Managers Table

## [unversioned]
See the git log.
