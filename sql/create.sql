DROP MATERIALIZED VIEW IF EXISTS managers;
DROP MATERIALIZED VIEW IF EXISTS volume;
DROP MATERIALIZED VIEW IF EXISTS promiscuity;
DROP VIEW IF EXISTS volume1;
DROP VIEW IF EXISTS volume7;
DROP VIEW IF EXISTS volume30;
DROP VIEW IF EXISTS delegate_clients;
DROP TABLE IF EXISTS blocks;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS contracts;
DROP TABLE IF EXISTS similarity;
DROP TABLE IF EXISTS delegates;

CREATE TABLE contracts (
  id          VARCHAR(100) PRIMARY KEY NOT NULL,
  manager     VARCHAR(100),
  json        JSONB NOT NULL,
  balance     BIGINT,
  counter     VARCHAR(100),
  delegate    VARCHAR(100),
  codesize    INTEGER,
  annotations TEXT[],
  primitives  JSONB,
  script      JSONB,
  spendable   BOOL,
  activated   TIMESTAMP WITH TIME ZONE,
  originated  TIMESTAMP WITH TIME ZONE
);

CREATE TABLE blocks (
  id          VARCHAR(100) PRIMARY KEY NOT NULL,
  ts          TIMESTAMP WITH TIME ZONE,
  predecessor VARCHAR(100) NOT NULL,
  json        JSONB NOT NULL
);

CREATE TABLE transactions (
  ophash      VARCHAR(100) NOT NULL,
  kind        VARCHAR(50) NOT NULL,
  ts          TIMESTAMP WITH TIME ZONE,
  src         VARCHAR(100),
  dst         VARCHAR(100),
  counter     INTEGER,
  idx         INTEGER,
  amount      BIGINT,
  status      VARCHAR(50),
  json        JSONB NOT NULL,
  PRIMARY KEY(ts, ophash, idx)
);

CREATE TABLE similarity (
  contract    VARCHAR(100) PRIMARY KEY NOT NULL,
  x DOUBLE PRECISION,
  y DOUBLE PRECISION,
  size INTEGER
);

CREATE TABLE delegates (
  address    VARCHAR(100) PRIMARY KEY NOT NULL,
  name       VARCHAR(50),
  stake      BIGINT
);

-- helper for volume in last 24 hours
CREATE OR REPLACE VIEW volume1 AS
SELECT contract,
SUM(amount) AS volume1
FROM ((
SELECT src AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '1' day
GROUP  BY src)
UNION (
SELECT dst AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '1' day
GROUP  BY dst)) AS one
GROUP  BY contract;

-- helper for volume in last 7 days
CREATE OR REPLACE VIEW volume7 AS
SELECT contract,
SUM(amount) AS volume7
FROM ((
SELECT src AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '7' day
GROUP  BY src)
UNION (
SELECT dst AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '7' day
GROUP  BY dst)) AS seven
GROUP  BY contract;

-- helper for volume in last 30 days
CREATE OR REPLACE VIEW volume30 AS
SELECT contract,
SUM(amount) AS volume30
FROM ((
SELECT src AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '30' day
GROUP  BY src)
UNION (
SELECT dst AS contract,
SUM(amount) AS amount
FROM transactions
WHERE kind = 'transaction'
AND ts > current_date - interval '30' day
GROUP  BY dst)) AS thirty
GROUP  BY contract;

-- the aggregate volume table
CREATE MATERIALIZED VIEW volume AS
SELECT
thirty.contract,
COALESCE(one.volume1, 0) AS volume1,
COALESCE(seven.volume7, 0) AS volume7,
COALESCE(thirty.volume30, 0) AS volume30
FROM
volume30 thirty
LEFT OUTER JOIN volume1 AS one ON one.contract = thirty.contract
LEFT OUTER JOIN volume7 AS seven ON seven.contract = thirty.contract;

-- managers view
-- SELECT * FROM managers;
CREATE MATERIALIZED VIEW managers AS
SELECT m.id, c.activated, COALESCE(c.balance, 0) as ownbalance, m.totalbalance,
COALESCE(v.volume1, 0) as volume1,
COALESCE(v.volume7, 0) as volume7,
COALESCE(v.volume30, 0) as volume30,
m.contracts
FROM
(SELECT manager as id, sum(balance) as totalbalance, count(*) - 1 as contracts
FROM contracts
GROUP BY manager ORDER BY totalbalance DESC) as m
LEFT OUTER JOIN contracts c ON c.id = m.id
LEFT OUTER JOIN volume v ON m.id = v.contract
ORDER BY totalbalance DESC;

-- promiscuity
-- SELECT id, sum_addresses FROM promiscuity ORDER BY sum_addresses DESC;
CREATE MATERIALIZED VIEW promiscuity AS
SELECT sa.id,
COALESCE(sent_transactions, 0) as sent_transactions,
COALESCE(rcvd_transactions, 0) as rcvd_transactions,
COALESCE(sent_transactions, 0) + COALESCE(rcvd_transactions, 0) as sum_transactions,
COALESCE(sent_addresses, 0) as sent_addresses,
COALESCE(rcvd_addresses, 0) as rcvd_addresses,
COALESCE(sent_addresses, 0) + COALESCE(rcvd_addresses, 0) as sum_addresses
FROM
(SELECT c.id, count(distinct(t.dst)) AS sent_addresses
FROM contracts c
JOIN transactions t ON c.id = t.src
WHERE t.kind = 'transaction'
GROUP BY(id)) as sa
LEFT OUTER JOIN
(SELECT c.id, count(distinct(t.src)) AS rcvd_addresses
FROM contracts c
JOIN transactions t ON c.id = t.dst
WHERE t.kind = 'transaction'
GROUP BY(id)) as ra ON ra.id = sa.id
LEFT OUTER JOIN
(SELECT c.id, count(c.id) AS sent_transactions
FROM contracts c
JOIN transactions t ON c.id = t.src
WHERE t.kind = 'transaction'
GROUP BY(id)) as st ON st.id = sa.id
LEFT OUTER JOIN
(SELECT c.id, count(c.id) AS rcvd_transactions
FROM contracts c
JOIN transactions t ON c.id = t.dst
WHERE t.kind = 'transaction'
GROUP BY(id)) as rt ON rt.id = sa.id
ORDER BY sum_addresses DESC;

--- delegate_clients
CREATE VIEW delegate_clients AS
SELECT
c.delegate,
count(distinct(c.manager)) - 1 as clients,
count(c.id) - 1 as accounts,
(count(c.id) -1)::float / NULLIF((count(distinct(c.manager)) - 1),0) as ratio
FROM contracts c
where c.delegate is not null
group by c.delegate;

-- indexes
CREATE INDEX ON transactions (src);
CREATE INDEX ON transactions (dst);
CREATE INDEX ON volume (volume1);
CREATE INDEX ON volume (volume7);
CREATE INDEX ON volume (volume30);
CREATE INDEX ON contracts (activated);
CREATE INDEX ON contracts (originated);
CREATE INDEX ON contracts (balance);


