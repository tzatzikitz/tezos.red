import React, { Component } from 'react'
import { connect } from 'react-redux'
import './Contracts.css'
import Table from './Table'
const { array, func, object } = require('prop-types')


class ContractList extends Component {
  render() {
    return (
      <Table contracts={this.props.contracts} matchParams={this.props.match.params} />
    );
  }

  static propTypes = {
    contracts: array,
    dispatch: func.isRequired,
    match: object.isRequired
  }
}

export default connect(
  state => ({
    contracts: state.contracts.items,
  })
)(ContractList)
