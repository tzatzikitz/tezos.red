import React, { Component } from 'react'
import { connect } from 'react-redux'
import Controls from './Controls'
import { Link } from 'react-router-dom'
import act from '../actions'
import Tree from '../tree/Tree'
import './Table.css'
import ReactTable from 'react-table'
import RelativeDate from '../common/relative-date'
import volumeColumns from '../common/table-headers'
import Balance from '../common/Balance'
import '../styles/ReactTable.css'
const { Grid } = require('react-bootstrap')
const { array, bool, func, number, object } = require('prop-types')


class Table extends Component {
  componentDidMount() {
    this.props.applyFilter(this.props.matchParams)
  }

  componentDidUpdate(prevProps) {
    if(prevProps.matchParams !== this.props.matchParams) {
      this.props.applyFilter(this.props.matchParams)
    }
  }

  get columns() {
    const { annotations, primitives } = this.props.columns
    let result = [
      {
        Header: 'Contract',
        columns: [
          {
            Header: 'Address',
            accessor: 'id',
            Cell: row => <Link to={`/${row.value}`}>{row.value}</Link>,
            minWidth: 340
          },
          {
            Header: 'Balance',
            accessor: 'balance',
            Cell: row => <Balance balance={row.value} />,
            minWidth: 130
          },
          {
            Header: 'Originated',
            accessor: 'originated',
            Cell: row => <RelativeDate date={row.value} />,
            minWidth: 120
          }
        ]
      },
      {
        Header: 'Delegate',
        accessor: 'delegate',
        Cell: row => <Link to={`/${row.value.address}`}>{row.value.name || row.value.address}</Link>,
        minWidth: 340
      },
      volumeColumns,
      {
        Header: 'Manager',
        columns: [
          {
            Header: 'Address',
            accessor: 'manager.id',
            minWidth: 340
          },
          {
            Header: 'Balance',
            accessor: 'manager.ownBalance',
            Cell: row => <Balance balance={row.value} />,
          }
        ]
      },
      {
        Header: 'Code',
        columns: [
          {
            Header: 'Size',
            accessor: 'codesize'
          },
        ]
      }
    ]
    if (annotations) result[result.length - 1].columns.push({
      Header: 'Annotations',
      accessor: 'annotations',
      Cell: row => (row.value && <ul className="inline">
        <li><i>{row.value.length}: </i></li>
        {row.value.map((a, i) => <li key={i}>{a}</li>)}
      </ul>)
    })
    if (primitives) result[result.length - 1].columns.push({
      Header: 'Primitives',
      accessor: 'primitives',
      Cell: row => row.value && <span>
        <i>{row.value.storage.length}: </i>
        <Tree className='tree' obj={row.value.storage} />
      </span>
    })
    return result
  }

  fetchData = (state) => {
    this.props.turnPage(
      state.page,
      state.pageSize,
      state.sorted)
  }

  render() {
    const { contracts, loading, pages } = this.props
    return (
      <Grid style={{marginTop: '65px'}} fluid>
        <Controls
          filter={this.props.filter}
          onFilter={this.props.toggleFilter}
          countAll={this.props.total}
          count={this.props.filtered} />
        <ReactTable
          manual
          onFetchData={this.fetchData}
          multiSort={false}
          data={contracts}
          columns={this.columns}
          loading={loading}
          pages={pages}
          pageSizeOptions={[5, 10, 15, 20, 25, 50, 100]}
          defaultPageSize={20}
          defaultSorted={[{ id: 'volume.7', desc: true }]}
          showPaginationTop
          showPaginationBottom={false}
          className="-striped -highlight"
        />
      </Grid>
    )
  }

  static propTypes = {
    contracts: array,
    filter: object,
    matchParams: object,
    toggleFilter: func,
    applyFilter: func,
    turnPage: func,
    columns: object,
    loading: bool,
    pages: number,
    total: number,
    filtered: number
  }

  static defaultProps = {
    contracts: []
  }
}

const mapStateToProps = state => ({
  filter: state.contracts.filter,
  columns: state.contracts.columns,
  contracts: state.contracts.items,
  pages: state.contracts.pages,
  loading: state.contracts.loading,
  total: state.contracts.total,
  filtered: state.contracts.filtered
})

export default connect(
  mapStateToProps,
  dispatch => ({
    turnPage: async (page, pageSize, sorted) => {
      await dispatch(act.contracts.turnPage(page, pageSize, sorted))
      await dispatch(act.contracts.fetch())
    },
    applyFilter: (params) => {
      let f = {}
      if (['xs', 'xe', 'ys', 'ye'].every(x => params[x])) {
        f.zoom = params
        f.type = 'smart'
      }
      if (params.manager) {
        f.manager = params.manager
      }
      dispatch(act.contracts.applyFilter(f))
      dispatch(act.contracts.fetch())
    }
  }))(Table)
