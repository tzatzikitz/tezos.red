import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Badge, Glyphicon, ButtonGroup, ToggleButton, ToggleButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap'
import './Controls.css'
import act from '../actions'
const { bool, number, func, object, string } = require('prop-types')

class Filter extends Component {
  handleClick = () => {
    this.props.resetFilter(this.props.type)
    this.props.history.push('/contracts')
  }
  render() {
    const { filter, type, label } = this.props
    const val = filter[type]
    if(!val) return null
    return <Badge title={`Reset filter ${label}`} onClick={this.handleClick} bsClass="badge filter">{label}
      <Glyphicon glyph="remove" />
    </Badge>
  }
  static propTypes = {
    label: string.isRequired,
    history: object.isRequired,
    resetFilter: func.isRequired,
    type: string.isRequired,
    filter: object.isRequired
  }
}

class Controls extends Component {
  get hasFilter() {
    return this.props.count !== this.props.countAll
  }

  renderCount() {
    return (
      <span>
        <strong>{this.props.count}</strong>
        {this.hasFilter && <span> of <strong>{this.props.countAll}</strong></span>}
      </span>
    )
  }
  render() {
    const { type, onFilterType, onFilterShowManagers, showManagers } = this.props
    return (
      <div className="controls">
        Displaying {this.renderCount()} contracts
        <ToggleButtonGroup className="toolbar"
          type="radio" name="type" value={type}
          onChange={onFilterType} >
          <ToggleButton value="all">All</ToggleButton>
          <ToggleButton value="smart">Smart</ToggleButton>
          <ToggleButton value="dumb">Basic</ToggleButton>
        </ToggleButtonGroup>

        <ToggleButtonGroup className="toolbar"
          type="checkbox" name="showManagers" value={[ showManagers && 1 ]}
          onChange={onFilterShowManagers} >
          <ToggleButton disabled={type === 'smart'} value={1}>Managers</ToggleButton>
        </ToggleButtonGroup>

        <ButtonGroup className="toolbar">
          <DropdownButton id='controls-columns' title={'Columns'}>
            <MenuItem
              onClick={() => this.props.toggleColumn('annotations')}
              active={this.props.columns.annotations}>
              Annotations</MenuItem>
            <MenuItem
              onClick={() => this.props.toggleColumn('primitives')}
              active={this.props.columns.primitives}>
              Primitives</MenuItem>
          </DropdownButton>
        </ButtonGroup>

        <Filter
          filter={this.props.filter}
          type="zoom"
          resetFilter={this.props.resetFilter}
          history={this.props.history}
          label="Similarity Map filter" />
      </div>
    )
  }

  static propTypes = {
    count: number,
    countAll: number,
    filter: object,
    history: object,
    columns: object,
    onFilter: func,
    toggleColumn: func,
    onFilterType: func,
    resetFilter: func,
    onFilterShowManagers: func,
    type: string,
    showManagers: bool
  }
}

export default connect(
  state => ({
    columns: state.contracts.columns,
    type: state.contracts.filter.type,
    showManagers: state.contracts.filter.showManagers
  }),
  dispatch => ({
    onFilterType: async type => {
      await dispatch(act.contracts.applyFilter({ type }))
      await dispatch(act.contracts.fetch())
    },
    onFilterShowManagers: async x => {
      await dispatch(act.contracts.applyFilter({ showManagers: !!x.length }))
      await dispatch(act.contracts.fetch())
    },
    toggleColumn: column => dispatch(act.contracts.toggleColumn(column)),
    resetFilter: async (filter, value) => {
      await dispatch(act.contracts.resetFilter(filter, value))
      await dispatch(act.contracts.fetch())
    }
  }))(withRouter(Controls))
