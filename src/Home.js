import React, { Component } from 'react'
import { Jumbotron, Grid, Panel, Row, Col, Image } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './Home.css'


export default class Home extends Component {
  render() {
    return (
      <Grid style={{ marginTop: '65px' }}>
        <Jumbotron>
          <h1 className="red">Tezos.red</h1>
          <p>
            A collection of tools for exploring and interacting with the Tezos blockchain.
          </p>
          <Row>
            <Link to="/contracts">
              <Col md={6}>
                <Panel>
                  <Panel.Heading>
                    <Panel.Title componentClass="h3">Contracts Explorer</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body>
                    <Image src="Contracts.png" thumbnail />
                  </Panel.Body>
                </Panel>
              </Col>
            </Link>
            <Col md={6}>
              <Panel>
                <Panel.Heading>
                  <Panel.Title componentClass="h3">Charts</Panel.Title>
                </Panel.Heading>
                <Panel.Body>Coming soon...</Panel.Body>
              </Panel>
              <Panel>
                <Panel.Heading>
                  <Panel.Title componentClass="h3">Alerts</Panel.Title>
                </Panel.Heading>
                <Panel.Body>Coming soon...</Panel.Body>
              </Panel>
              <Panel>
                <Panel.Heading>
                  <Panel.Title componentClass="h3">Tezos World Map</Panel.Title>
                </Panel.Heading>
                <Panel.Body>Coming soon...</Panel.Body>
              </Panel>
              <Panel>
                <Panel.Heading>
                  <Panel.Title componentClass="h3">Vote on Features</Panel.Title>
                </Panel.Heading>
                <Panel.Body>Coming soon...</Panel.Body>
              </Panel>
            </Col>
          </Row>
        </Jumbotron>
      </Grid>
    )
  }
}
