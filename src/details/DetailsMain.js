import React, { PureComponent } from 'react'
import { Glyphicon, Panel } from 'react-bootstrap'
import './Details.css'
import DataRow from './DataRow.js'
import { Link } from 'react-router-dom'
import Balance from '../common/Balance'
import RelativeDate from '../common/relative-date'
const { array, object } = require('prop-types')


class DetailsMain extends PureComponent {
  render() {
    const { details } = this.props
    const contract = details.id
    const {
      activated,
      balance,
      delegate,
      originated,
      volume = {}
    } = this.props.details || {}

    return (
      <Panel className="details">
        <Panel.Heading>
          <Panel.Title componentClass="h3">
            { contract }
            <a
              href={'https://tzscan.io/' + contract}
              title="Open on tzscan.io" rel="noopener noreferrer" target="_blank" className="pull-right">
              <Glyphicon glyph="new-window" /></a>
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          {originated && <DataRow label="Originated"><RelativeDate date={originated} /></DataRow>}
          {activated && <DataRow label="Activated"><RelativeDate date={activated} /></DataRow>}
          <DataRow label="Balance"><Balance balance={balance} /></DataRow>
          <hr/>
          <DataRow label="Volume&nbsp;24h"><Balance balance={volume['1']} /></DataRow>
          <DataRow label="Volume&nbsp;7d"><Balance balance={volume['7']} /></DataRow>
          <DataRow label="Volume&nbsp;30d"><Balance balance={volume['30']} /></DataRow>
          <hr/>
          {delegate.address && <DataRow label="Delegate">
            <Link to={`/${delegate.address}`}>{delegate.name || delegate.address}</Link>
          </DataRow>}
        </Panel.Body>
      </Panel>
    )
  }

  static propTypes = {
    details: object,
    contracts: array
  }

  static defaultProps = {
    contracts: []
  }
}

export default DetailsMain
