import React, { PureComponent } from 'react'
import { Glyphicon, Panel, Table } from 'react-bootstrap'
import DataRow from './DataRow.js'
import { Link } from 'react-router-dom'
import Balance from '../common/Balance'
const { object } = require('prop-types')


class Manager extends PureComponent {
  render() {
    const { manager } = this.props
    let { contracts = [] } = manager
    const { id } = manager
    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">Manager
            <a
              href={'https://tzscan.io/' + id}
              title="Open on tzscan.io" rel="noopener noreferrer" target="_blank" className="pull-right">
              <Glyphicon glyph="new-window" /></a>
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <DataRow label="Address">
            <Link to={`/${manager.id}`}>{manager.id}</Link>
          </DataRow>
          <DataRow label="Balance">
            <Balance style={{ marginRight: '5px' }} balance={manager.ownBalance} />,
              including contracts: <Balance balance={manager.totalBalance} />
          </DataRow>
          {(!!contracts.length) &&
           <DataRow label="Contracts">
             <Table striped condensed hover>
               <thead>
                 <tr>
                   <th>Address</th>
                   <th>Balance</th>
                 </tr>
               </thead>
               <tbody>
                 {contracts.map((c, idx) => (
                   <tr key={idx}>
                     <td>
                       <Link to={`/${c.id}`}>{c.id}</Link>
                     </td>
                     <td><Balance balance={c.balance} /></td>
                   </tr>))}
               </tbody>
             </Table>
             <ul>
             </ul>
           </DataRow>}
        </Panel.Body>
      </Panel>
    )
  }

  static propTypes = {
    manager: object
  }
  static defaultProps = {
    manager: {}
  }

}

export default Manager
