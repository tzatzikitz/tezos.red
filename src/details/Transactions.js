import React, { PureComponent } from 'react'
import { Panel } from 'react-bootstrap'
import ReactTable from 'react-table'
import RelativeDate from '../common/relative-date'
import Balance from '../common/Balance'
import { Link } from 'react-router-dom'
import act from '../actions'
import { connect } from 'react-redux'
const { array, func, bool, number, string } = require('prop-types')


class Transactions extends PureComponent {
  get columns() {
    let result = [
      {
        accessor: 'src',
        Header: 'From',
        Cell: row => <Link to={`/${row.value}`}>{row.value}</Link>,
      },
      {
        accessor: 'dst',
        Header: 'To',
        Cell: row => <Link to={`/${row.value}`}>{row.value}</Link>,
      },
      {
        accessor: 'ts',
        Header: 'Date',
        Cell: row => <RelativeDate date={row.value} />,
      },
      {
        accessor: 'amount',
        Header: 'Amount',
        Cell: row => <Balance balance={Number(row.value)} />
      }
    ]
    return result
  }

  fetchData = (state) => {
    this.props.fetchTransactions(
      this.props.contract,
      state.pageSize,
      state.page,
      state.sorted,
      state.filtered)
  }

  render() {
    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">Transactions</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <ReactTable
            manual
            onFetchData={this.fetchData}
            multiSort={false}
            data={this.props.transactions}
            columns={this.columns}
            loading={false}
            pages={this.props.pages}
            pageSizeOptions={[5, 10, 15, 20, 25, 50, 100]}
            defaultPageSize={5}
            defaultSorted={[{ id: 'ts', desc: true }]}
            showPaginationTop
            showPaginationBottom={false}
            className="-striped -highlight" />
        </Panel.Body>
      </Panel>)
  }

  static propTypes = {
    transactions: array,
    fetchTransactions: func,
    loading: bool,
    contract: string,
    pages: number,
  }
}

const mapStateToProps = state => ({
  transactions: state.details.transactions.items,
  pages: state.details.transactions.pages,
  loading: state.details.transactions.loading
})

const mapDispatchToProps = dispatch => ({
  fetchTransactions: (...params) => dispatch(act.details.fetchTransactions(...params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Transactions)
