import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { bool, string, func, object } from 'prop-types'
import { Panel, Glyphicon } from 'react-bootstrap'
import act from '../actions'
import DetailsMain from './DetailsMain'
import DetailsManager from './manager'
import DetailsSmart from './Smart'
import Transactions from './Transactions'
import { Grid, Row, Col } from 'react-bootstrap'
import DetailsGraph from '../graph/details'


class Details extends PureComponent {
  componentDidMount() {
    const { contract } = this.props.match.params
    this.props.dispatch(act.details.fetch(contract))
  }

  componentDidUpdate(prevProps) {
    const current = prevProps.match.params.contract
    const next = this.props.match.params.contract
    if (current !== next) {
      this.props.dispatch(act.details.fetch(next))
    }
  }

  render () {
    const { details = {}, status, match, loading } = this.props
    const { contract } = match.params
    const { manager } = details

    if (loading) return null

    if (status === 'NOTFOUND') return (
      <Grid style={{ marginTop: '65px' }}>
        <Row>
          <Panel>
            <Panel.Heading>
              <Panel.Title componentClass="h3">Contract Not Found</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <p>
                The contract <i>{contract}</i> was not found by our node.
              </p>
              <p>
                Try your luck on
                <a
                  href={'https://tzscan.io/' + contract}
                  style={{ paddingLeft: '5px' }}
                  title="Open on tzscan.io" rel="noopener noreferrer" target="_blank">
                  <Glyphicon glyph="new-window" /> tzscan</a> instead.
              </p>
            </Panel.Body>
          </Panel>
        </Row>
      </Grid>
    )
    if (!Object.keys(details).length) return null

    if (details.script) {
      return <DetailsSmart contract={contract} details={details} />
    } else {
      return (
        <Grid fluid style={{ marginTop: '65px' }}>
          <Row>
            <Col md={6}>
              <DetailsMain details={details} />
              <Transactions contract={contract} />
            </Col>
            <Col md={6}>
              <DetailsManager manager={manager} />
              <DetailsGraph contract={contract} />
            </Col>
          </Row>
        </Grid>)
    }
  }

  static propTypes = {
    details: object,
    status: string,
    match: object.isRequired,
    dispatch: func.isRequired,
    loading: bool
  }
}

export default connect(
  state => ({
    status: state.details.result.status,
    details: state.details.result.result,
    loading: state.details.loading,
  }))(Details)
