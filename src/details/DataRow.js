import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { any, string } from 'prop-types'
import ToggleJson from './ToggleJson'
import './DataRow.css'

const DataRow = ({ label, children, json }) => {
  return (
    <Row className="dataRow">
      <Col lg={2}>
        <strong>{label}</strong><br/>
        {json && <ToggleJson obj={json} label={label} />}
      </Col>
      <Col lg={10}>{children}</Col>
    </Row>)
}

DataRow.propTypes = {
  label: string,
  children: any,
  json: any
}

export default DataRow
