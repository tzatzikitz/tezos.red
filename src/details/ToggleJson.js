import React, { Component, PureComponent } from 'react'
import { Button, Modal } from 'react-bootstrap'
const { any, string } = require('prop-types')


class JsonModal extends PureComponent {
  render() {
    return (
      <Modal {...this.props} animation={false} bsSize="large">
        <Modal.Header closeButton>
          <Modal.Title>{this.props.label}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <pre>
            {this.props.children}
          </pre>
        </Modal.Body>
      </Modal>
    )
  }
}

class ToggleJson extends Component {
  constructor() {
    super()
    this.state = {
      show: false
    }
  }

  get repr() {
    return JSON.stringify(this.props.obj, null, 2)
  }

  toggle = () => {
    this.setState({
      show: !this.state.show
    })
  }

  render() {
    return (
      <span>
        <Button bsSize="xsmall" onClick={this.toggle}>
          {'{...}'}
        </Button>
        <JsonModal label={this.props.label} show={this.state.show} onHide={this.toggle}>
          {this.repr}
        </JsonModal>
      </span>
    )
  }

  static propTypes = {
    obj: any,
    label: string
  }
}

export default ToggleJson
