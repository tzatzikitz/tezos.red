import React, { PureComponent } from 'react'
import DetailsMain from './DetailsMain'
import DetailsManager from './manager'
import DetailsGraph from '../graph/details'
import DetailsParams from './DetailsParams'
import { Grid, Row, Col, Panel } from 'react-bootstrap'
import { getCode } from '../common/utils'
import Tree from '../tree/Tree'
import ToggleJson from './ToggleJson'
import Transactions from './Transactions'
import { object } from 'prop-types'
import DataRow from './DataRow.js'
const { size } = require('../common/size')


class DetailsCode extends PureComponent {
  render() {
    const { details } = this.props
    const src = details && getCode(details.script)

    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">Code</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <DataRow label="Code size">{size(src)}</DataRow>
          <Tree obj={src} /><br/>
          <ToggleJson obj={src} />
        </Panel.Body>
      </Panel>)
  }

  static propTypes = {
    details: object
  }
}

class DetailsStorage extends PureComponent {
  render() {
    const { details } = this.props
    const { script } = details
    const storage = script && script.storage

    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">
            Storage
          </Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <Tree obj={storage} />
          <ToggleJson obj={storage} label="Storage" />
        </Panel.Body>
      </Panel>)
  }

  static propTypes = {
    details: object
  }
}


export default class DetailsSmart extends PureComponent {
  render() {
    const { details, contract } = this.props
    const { manager } = details
    return (
      <Grid style={{ marginTop: '65px' }} fluid>
        <Row>
          <Col md={6}>
            <DetailsMain details={details} />
            <Transactions contract={contract} />
          </Col>
          <Col md={6}>
            <DetailsManager manager={manager} />
            <DetailsGraph contract={details.id} />
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <DetailsParams details={details} />
          </Col>
          <Col md={6} id="storage">
            <DetailsStorage details={details} />
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <DetailsCode details={details} />
          </Col>
        </Row>
      </Grid>)
  }

  static propTypes = {
    details: object
  }
}
