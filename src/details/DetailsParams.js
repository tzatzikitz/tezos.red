import React, { PureComponent } from 'react'
import { Panel } from 'react-bootstrap'
import Tree from '../tree/Tree'
import DataRow from './DataRow.js'
import { getCode } from '../common/utils'
const { object } = require('prop-types')


class DetailsParams extends PureComponent {
  render() {
    const { script } = this.props.details || {}
    const parameter = getCode(script, 'parameter')
    const storage = getCode(script, 'storage')
    const ret = getCode(script, 'return')

    return (
      <Panel>
        <Panel.Heading>
          <Panel.Title componentClass="h3">Types</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <DataRow label="Parameter" json={parameter}>
            <Tree obj={parameter} />
          </DataRow>
          <DataRow label="Storage" json={storage}>
            <Tree obj={storage}/>
          </DataRow>
          <DataRow label="Return" json={ret}>
            <Tree obj={ret} />
          </DataRow>
        </Panel.Body>
      </Panel>
    )
  }

  static propTypes = {
    details: object
  }
}

export default DetailsParams
