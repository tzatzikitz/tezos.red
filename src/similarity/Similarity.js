import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { object, func } from 'prop-types'
import act from '../actions'
const Rainbow = require('rainbowvis.js')
const { Chart, Scatter } = require('react-chartjs-2')
const zoomPlugin = require('chartjs-plugin-zoom')
const { values } = Object
const { options } = require('./chartOptions')


class Similarity extends Component {
  componentDidMount() {
    Chart.pluginService.register(zoomPlugin)
    this.props.dispatch(act.similarity.fetch())
  }

  scaleSize(data) {
    const rainbow = new Rainbow()
    const sizes = data.map(d => d.codesize)
    const min = 0
    const max = Math.max(...sizes)
    rainbow.setNumberRange(min, max)
    rainbow.setSpectrum('#00BF92', '#E50035')
    return data.map(d => {
      const color = '#' + rainbow.colorAt(d.codesize)
      return color
    })
  }

  get data() {
    const ds = values(this.props.similarity)
    return {
      datasets: [
        {
          pointBackgroundColor: ds.length ? this.scaleSize(ds) : '',
          pointHoverBorderWidth: 10,
          pointRadius: 4,
          pointHitRadius: 6,
          data: ds
        }
      ]
    }
  }

  clickHandler = (event, [target]) => {
    if (target) {
      const idx = target._index
      const contract = this.data.datasets[0].data[idx].id
      this.props.history.push(`/${contract}`)
    }
  }

  handleZoom() {
    const x = this.chart.chartInstance.scales['x-axis-1']
    const y = this.chart.chartInstance.scales['y-axis-1']

    this.props.dispatch(act.similarity.zoom({
      xs: x.start,
      xe: x.end,
      ys: y.start,
      ye: y.end
    }))
  }

  refChart = (ref) => { this.chart = ref }

  render() {
    return (
      values(this.props.similarity).length ?
        <Scatter
          ref={this.refChart}
          options={options({
            onClick: this.clickHandler,
            onZoom: this.handleZoom.bind(this),
            onPan: this.handleZoom.bind(this)
          })}
          width={400}
          height={400}
          data={this.data} /> : null)
  }

  static propTypes = {
    dispatch: func.isRequired,
    similarity: object.isRequired,
    history: object.isRequired,
  }
}

const mapStateToProps = state => ({
  similarity: state.similarity.data,
})

export default connect(mapStateToProps)(withRouter(Similarity))
