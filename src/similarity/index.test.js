import { inRange } from '.'
const { expect } = require('chai')


it('inRange', () => {
  const zoom = {
    xs: 0, xe: 1,
    ys: 2, ye: 3
  }
  expect(inRange({ x: 1, y: 2 }, zoom)).to.be.true
  expect(inRange({ x: 1.01, y: 2 }, zoom)).to.be.false
});
