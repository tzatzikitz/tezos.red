module.exports = {
  options: ({ onZoom, onPan, onClick }) => ({
    pan: {
      onPan,
      enabled: true,
      mode: 'xy',
      rangeMin: {
        x: -1.1,
        y: -1.1
      },
      rangeMax: {
        x: 1.1,
        y: 1.1
      }
    },
    zoom: {
      onZoom,
      enabled: true,
      mode: 'xy',
      rangeMin: {
        x: -1.1,
        y: -1.1
      },
      rangeMax: {
        x: 1.1,
        y: 1.1
      }
    },
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        display: false,
        gridLines: {
          display:false
        }
      }],
      yAxes: [{
        display: false,
        gridLines: {
          display:false,
          drawTicks: false
        }
      }]
    },
    tooltips: {
      enabled: false
    },
    animation: false,
    onClick
  })
}
