function inRange(obj, zoom) {
  return (obj.x >= zoom.xs) && (obj.x <= zoom.xe) && (obj.y >= zoom.ys) && (obj.y <= zoom.ye)
}

function rangeFilter(zoom) {
  return obj => inRange(obj, zoom)
}

module.exports = {
  inRange,
  rangeFilter
}
