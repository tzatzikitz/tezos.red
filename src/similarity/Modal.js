import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Similarity from './Similarity'
import { rangeFilter } from '.'
import AboutSimilarity from './About'
import { bool, number, object, func } from 'prop-types'
import './Modal.css'
const { values } = Object


class SimilarityModal extends Component {
  constructor() {
    super()
    this.state = {
      about: false
    }
  }

  get filterLink() {
    const z = this.props.zoom
    return '/contracts/' + [z.xs, z.xe, z.ys, z.ye].join('/')
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onHide} autoFocus animation={false} >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-sm">
            Smart Contract Similarity Map
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.about ?
            <AboutSimilarity /> :
            <Similarity />}
        </Modal.Body>
        <Modal.Footer>
          <Link className="pull-left" to={this.filterLink}>
            <Button onClick={this.props.onHide}>Filter {this.props.count} contracts</Button>
          </Link>
          <Button onClick={() => this.setState({ about: !this.state.about })}>Help</Button>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  static propTypes = {
    onHide: func.isRequired,
    count: number.isRequired,
    show: bool.isRequired,
    dispatch: func.isRequired,
    zoom: object.isRequired
  }
}

export default connect(state => {
  const { zoom, data } = state.similarity
  const count = values(data).filter(rangeFilter(zoom)).length
  return { zoom, count }
})(SimilarityModal)
