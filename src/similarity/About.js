import React from 'react'

const AboutSimilarity = () => {
  return (<div>
    <p>
      The scatter plot represents similarities between the code of the contracts.
      Points closer together contain more similar Michelson stack elements.
      The more of the same code constructs two contracts use, the closer they are together.
    </p>
    <p>
      {'The color coding represents the size of a contracts code.'}
    </p>
    <p>
      Click a point for details.
    </p>
  </div>)
}

export default AboutSimilarity
