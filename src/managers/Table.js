import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import act from '../actions'
import RelativeDate from '../common/relative-date'
import Balance from '../common/Balance'
import volumeColumns from '../common/table-headers'
const { Grid } = require('react-bootstrap')
const { array, bool, func, number } = require('prop-types')


class Table extends Component {
  get columns() {
    let result = [
      {
        Header: 'Manager',
        columns: [
          {
            accessor: 'id',
            Header: 'Address',
            minWidth: 230,
            Cell: row => <Link to={`/${row.value}`}>{row.value}</Link>,
          },
          {
            Header: 'Activated',
            accessor: 'activated',
            Cell: row => <RelativeDate date={row.value} />,
            minWidth: 120
          }
        ]
      },
      {
        Header: 'Balance',
        columns: [
          {
            accessor: 'ownBalance',
            Header: 'Own',
            Cell: row => <Balance balance={row.value} />
          },
          {
            accessor: 'totalBalance',
            Header: 'Incl. contracts',
            Cell: row => <Balance balance={row.value} />
          }
        ]
      },
      volumeColumns,
      {
        Header: 'Contracts',
        accessor: 'contracts',
        Cell: row => <Link to={`/contracts/manager/${row.original.id}`}>{row.value}</Link>,
      },
    ]
    return result
  }

  fetchData = (state) => {
    this.props.fetchManagers(
      state.pageSize,
      state.page,
      state.sorted,
      state.filtered)
  }

  render() {
    const { loading, pages } = this.props
    return (
      <Grid style={{marginTop: '65px'}} fluid>
        <ReactTable
          manual
          onFetchData={this.fetchData}
          multiSort={false}
          data={this.props.managers}
          columns={this.columns}
          loading={loading}
          pages={pages}
          pageSizeOptions={[5, 10, 15, 20, 25, 50, 100]}
          defaultPageSize={20}
          defaultSorted={[{ id: 'totalBalance', desc: true }]}
          showPaginationTop
          showPaginationBottom={false}
          className="-striped -highlight" />
      </Grid>
    )
  }

  static propTypes = {
    managers: array,
    fetchManagers: func,
    loading: bool,
    pages: number,
  }
}

const mapStateToProps = state => ({
  managers: state.managers.items,
  pages: state.managers.pages,
  loading: state.managers.loading
})

const mapDispatchToProps = dispatch => ({
  fetchManagers: (...params) => dispatch(act.managers.fetch(...params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Table)
