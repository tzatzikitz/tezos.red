import React, { Component, PureComponent } from 'react'
import { connect } from 'react-redux'
import act from '../actions'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import Leaflet from 'leaflet'
import './Map.css'
import { array, func, string, number } from 'prop-types'
import { HashLink as Link } from 'react-router-hash-link'
const { Glyphicon, Tab, Tabs, Grid, Table: BsTable, Button, Modal, FormGroup, ControlLabel, FormControl } = require('react-bootstrap')

const CONTRACT = 'KT1DLw2U9kWVhvRjBo6yjmgvj4gn8WWRbaVL'

const marker = Leaflet.Icon.extend({
  options: {
    iconUrl: require('./marker.png'),
    iconSize:    [25, 41],
    iconAnchor:  [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize:  [41, 41]
  }
})

const About = () => {
  return (<div className="about">
    The map is an example of an application that uses a <Link smooth to={`/${CONTRACT}#storage`}>smart contract</Link> on
    the Tezos blockchain to store, update and delete data. The <a href="https://gitlab.com/tzatzikitz/tezos.red/blob/master/contracts/world-map.liq">liquidity source</a> of the contract
    can be found in the project&apos;s repository.
  </div>)
}

class MyModal extends React.Component {
  render () {
    const { lat, lng } = this.props
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Add map entry</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddEntry lat={lat} lng={lng}/>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  static propTypes = {
    lat: number,
    lng: number,
    onHide: func
  }
}

class DeleteModal extends React.Component {
  render () {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Delete entry</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Sender must be <b>{this.props.owner}</b> for this operation to succeed.</p>
          <DeleteInstructions name={this.props.name} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  static propTypes = {
    owner: string,
    name: string,
    onHide: func
  }
}

class Instructions extends PureComponent {
  escape (text) {
    return text
      .replace(/'/g, '\'\\\'\'')
  }
  renderNode() {
    return <p> Call this command with the --dry-run option included to display the additional storage fees. Run without --dry-run to broadcast the operation. </p>
  }

  renderWallets() {
    return <p> Send at least <b>{this.sum}</b> ꜩ to <b>{CONTRACT}</b> including the parameters: </p>
  }

  render() {
    return <Tabs defaultActiveKey={1} id="ui-choice">
      <Tab eventKey={1} title="Node">
        {this.renderNode()}
        <pre>
          tezos-client transfer {this.sum} from <i>WALLET</i> to {CONTRACT} <b>--dry-run</b> --fee 0 --arg \<br/>
          &apos;{this.params(this.escape)}&apos;
        </pre>
      </Tab>
      <Tab eventKey={3} title="Other Wallets">
        {this.renderWallets()}
        <pre>
          {this.params()}
        </pre>
      </Tab>
    </Tabs>
  }
}

class AddInstructions extends Instructions {
  sum = 0.5
  params(escape=x=>x) {
    let { lat, lng, name, text } = this.props
    text = text.replace(/\n/g, '\\n')
    return '(Pair "add" (Pair "'+ name + '" (Some (Pair "' + lat + '" (Pair "' + lng + '" "' + escape(text) + '")))))'
  }
}

class DeleteInstructions extends Instructions {
  sum = 0
  params() {
    let { name } = this.props
    return '(Pair "delete" (Pair "'+ name + '" None))'
  }
}

class AddEntry extends Component {
  state = {
    name: '',
    text: '',
    sum: 0.5
  }

  handleChangeName = e => {
    this.setState({ name: e.target.value });
  }

  handleChangeText = e => {
    this.setState({ text: e.target.value });
  }

  render() {
    const { lat, lng } = this.props
    const { name, text } = this.state
    return <div><form>
      <FormGroup
        controlId="formBasicText" >
        <ControlLabel>Displayed name</ControlLabel>
        <FormControl
          type="text"
          value={this.state.name}
          onChange={this.handleChangeName} />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Text</ControlLabel>
        <FormControl componentClass="textarea"
          onChange={this.handleChangeText} />
      </FormGroup>
    </form>
    <AddInstructions name={name} text={text} lat={lat} lng={lng} />
    <p>
      It can take a few minutes for tezos.red to update the contract storage information and for your entry to appear.
    </p>
    </div>
  }

  static propTypes = {
    lat: number,
    lng: number
  }
}

class MyMap extends Component {
  state = {
    lat: 48.20849496595639,
    lng: 16.3722833711654,
    zoom: 2,
    modalShow: false,
    deleteModalShow: false,
  }

  componentDidMount() {
    this.props.fetch(CONTRACT)
  }

  handleMoveEnd = () => {
    let center = this.map.leafletElement.getCenter()
    let zoom = this.map.leafletElement.getZoom()
    this.setState({ lat: center.lat, lng: center.lng, zoom: zoom })
  }

  setRef = r => this.map = r

  modalClose = () => this.setState({ modalShow: false })
  showAddModal = () => this.setState({ modalShow: true })
  deleteModalClose = () => this.setState({ deleteModalShow: false })
  showDeleteModal = (name, owner) => this.setState({
    deleteModalShow: true,
    deleteOwner: owner,
    deleteName: name
  })

  getMarkers (storage) {
    if (!storage) return []
    return storage.map(({ args }) => {
      const result = {
        name: args[0].string,
        owner: args[1].args[1].string,
        position: {
          lat: Number(args[1].args[0].args[0].string),
          lng: Number(args[1].args[0].args[1].args[0].string),
        },
        text: args[1].args[0].args[1].args[1].string
      }
      return result
    })
  }

  render() {
    const position = [this.state.lat, this.state.lng]
    const { storage } = this.props
    return (
      <div>
        <Map
          style={{
            height: '640px',
            width: window.innerWidth + 'px',
            marginBottom: '10px'
          }}
          className="myMap"
          center={position}
          zoom={this.state.zoom}
          ref={this.setRef}
          onMoveEnd={this.handleMoveEnd} >
          <TileLayer
            attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {this.getMarkers(storage).map((m, idx) =>
            <Marker
              key={idx}
              icon={new marker()}
              position={m.position}>
              <Popup>
                <h4>{m.name}</h4>
                {m.text}
              </Popup>
            </Marker>)}
          <Marker onClick={this.showAddModal} position={position}></Marker>
        </Map>
        <Grid>
          <div className="controls">
            <MyModal lat={this.state.lat} lng={this.state.lng} show={this.state.modalShow} onHide={this.modalClose} />
            <DeleteModal owner={this.state.deleteOwner} name={this.state.deleteName} show={this.state.deleteModalShow} onHide={this.deleteModalClose} />
            <BsTable striped bordered condensed hover className="">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Text</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {this.getMarkers(storage).map((m, idx) =>
                  <tr
                    key={idx}
                    position={m.position}>
                    <td>{m.name}</td>
                    <td>{m.text}</td>
                    <td>{m.position.lat}</td>
                    <td>{m.position.lng}</td>
                    <td>
                      <Button bsSize="xsmall" onClick={() => this.showDeleteModal(m.name, m.owner)} >
                        <Glyphicon glyph="remove" />
                      </Button>
                    </td>
                  </tr>)}
              </tbody>
            </BsTable>
          </div>
          <About />
        </Grid>
      </div>
    )
  }

  static propTypes = {
    storage: array,
    fetch: func
  }
}

const mapStateToProps = state => ({
  storage: state.details.result.result &&
           state.details.result.result.script &&
           state.details.result.result.script.storage,
})

const mapDispatchToProps = dispatch => ({
  fetch: (...params) => dispatch(act.details.fetch(...params))
})

export default connect(mapStateToProps, mapDispatchToProps)(MyMap)
