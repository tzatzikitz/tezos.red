import { combineReducers } from 'redux'
import contracts from './contracts'
import managers from './managers'
import graph from './graph'
import delegates from './delegates'
import details from './details'
import meta from './meta'
import similarity from './similarity'

export default combineReducers({
  meta,
  contracts,
  managers,
  graph,
  delegates,
  similarity,
  details
})
