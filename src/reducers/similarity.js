const { SIMILARITY } = require('../constants')

const initial = {
  data: {},
  zoom: {
    xs: -1,
    xe: 1,
    ys: -1,
    ye: 1
  }
}

export default (state = initial, action) => {
  switch (action.type) {
  case SIMILARITY.RECEIVED:
    return {
      ...state,
      data: action.payload
    }
  case SIMILARITY.ZOOM:
    return {
      ...state,
      zoom: action.payload
    }
  default:
    return state
  }
}
