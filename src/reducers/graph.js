const { GRAPH } = require('../constants')
const { uniq } = require('../common/utils')
const { LAYOUTS } = require('../graph/cytoscape')

let elements = []

function lbl(id) {
  return id.slice(0, 7)// + '...' + id.slice(id.length - 4)
}

function clr(id, type) {
  if (type === 'delegate') return '#226666'
  return id.startsWith('tz') ? '#530000' : '#AA3939'
}

function cycle(list, current) {
  let pos = list.findIndex(x => x === current)
  if (pos === list.length - 1) return list[0]
  return list[pos + 1]
}

export const initial = {
  nodes: [],
  elements: [],
  core: [],
  collections: [],
  options: {
    layout: 'breadthfirst'
  },
  show: true,
  loading: false
}

export default (state = initial, action) => {
  function addNode(id, type, label, color, balance, totalBalance) {
    let e = elements.find(x => x.data.id === id)
    if (e) {
      if(type) e.classes += ` ${type}`
      if(label) e.data.label = label
      if(color) e.data.color = color
      if(balance) e.data.balance = balance
      if(totalBalance) e.data.totalBalance = totalBalance
      return
    }
    color = color || clr(id, type)
    if (core.includes(id)) {
      type += ' core'
    }
    elements.push({
      data: {
        id,
        color,
        type,
        shape: id.startsWith('tz') ? 'ellipse' : 'rectangle',
        label: label || lbl(id)
      },
      classes: 'multiline-manual ' + (type ? type : '')
    })
  }

  function addEdge(source, target, type, color) {
    elements.push({
      data: {
        id: `${source}${target}${type}${color}`,
        source,
        type,
        target,
        color },
      classes: type
    })
  }
  if (action.type === GRAPH.CORE.ADD) {
    const { core } = state
    if (core.includes(action.payload)) return state
    return {
      ...state,
      core: core.concat([ action.payload ])
    }
  }
  if (action.type === GRAPH.CORE.REMOVE) {
    let core = [ ...state.core ]
    if (!state.core.includes(action.payload)) return state
    const index = state.core.indexOf(action.payload)
    core.splice(index, 1)
    return {
      ...state,
      core
    }
  }
  if (action.type === GRAPH.TOGGLEPANEL) {
    return {
      ...state,
      show: !state.show
    }
  }
  if (action.type === GRAPH.OPTIONS.TOGGLELAYOUT) {
    return {
      ...state,
      options: {
        ...state.options,
        layout: cycle(Object.keys(LAYOUTS), state.options.layout)
      }
    }
  }
  if (action.type === GRAPH.COLLECTION.SAVE) {
    const { name, nodes } = action.payload
    const collections = [ ...state.collections || [] ]
    collections.push({ name, nodes })
    return {
      ...state,
      collections
    }
  }
  if (action.type === GRAPH.COLLECTION.LOAD) {
    const name = action.payload
    const collection = state.collections.find(c => c.name === name)
    return {
      ...state,
      core: collection.nodes
    }
  }
  if (action.type === GRAPH.COLLECTION.REMOVE) {
    const name = action.payload
    const collections = [ ...state.collections || [] ]
    const index = collections.findIndex(c => c.name === name)
    collections.splice(index, 1)
    return {
      ...state,
      collections
    }
  }
  if (action.type.startsWith('graph') && action.payload && action.payload.status !== 'OK') {
    console.warn('Server response is not OK', action.payload)
    return state
  }
  if (!action.payload) return state
  const { core, managers, delegates, transactions, details } = action.payload
  if (!managers) {
    elements = []
  } else {
    elements = []
    for (let [contract, manager] of managers) {
      if (contract === manager) continue
      if (!manager) continue
      addNode(contract)
      addNode(manager)
      addEdge(manager, contract, 'manager', '#a50707')
    }
    for (let [contract, delegate, name] of delegates) {
      if (!delegate) continue
      addNode(contract, 'delegee')
      addNode(delegate, 'delegate', name, '#226666')
      addEdge(contract, delegate, 'delegate', '#226666')
    }
    for (let { src, dst } of transactions) {
      addNode(src, 'sender')
      addNode(dst, 'receiver')
      addEdge(src, dst, 'sends', '#567714')
    }
    for (let { id, balance, totalBalance } of details) {
      addNode(id, null, null, null, balance, totalBalance)
    }
  }

  switch (action.type) {
  case GRAPH.RECEIVED:
    return {
      ...state,
      loading: false,
      elements,
      nodes: uniq(elements
        .filter(x => !x.data.source)
        .map(x => x.data.id))
    }
  default:
    return state
  }
}
