const { META } = require('../constants')

const initial = {
  data: {},
  loading: false
}

export default (state = initial, action) => {
  switch (action.type) {
  case META.RECEIVED:
    return {
      ...state,
      loading: false,
      data: action.payload
    }
  default:
    return state
  }
}
