const { DETAILS } = require('../constants')

const initial = {
  loading: false,

  result: {},

  transactions: {
    // items: [],
    // pages: 0,
    // loading: false
  }
}

export default (state = initial, action) => {
  if (action.type === DETAILS.FETCH) {
    return {
      ...state,
      loading: true
    }
  }
  else if (action.type === DETAILS.RECEIVED.DETAILS) {
    return {
      ...state,
      result: action.payload,
      loading: false
    }
  }
  else if (action.type === DETAILS.RECEIVED.TRANSACTIONS) {
    return {
      ...state,
      transactions: action.payload
    }
  }

  else return state
}
