const { MANAGERS } = require('../constants')

const initial = {
  items: [],
  pages: 0,
  loading: false
}

export default (state = initial, action) => {
  if (action.type.startsWith('managers') && action.payload && action.payload.status !== 'OK') {
    console.warn('Server response is not OK', action.payload)
    return state
  }
  if (!action.payload) return state
  const { items, pages, total, filtered } = action.payload
  switch (action.type) {
  case MANAGERS.RECEIVED:
    return { ...state, loading: false, items, pages, total, filtered }
  default:
    return state
  }
}
