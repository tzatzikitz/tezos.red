const { CONTRACTS } = require('../constants')

const defaultFilter = {
  type: CONTRACTS.TYPE.ALL,
  zoom: undefined,
  manager: undefined,
  showManagers: true
}

export const initial = {
  items: [],
  page: 0,
  pageSize: 20,
  sorted: [ { id: 'volume.7', desc: true } ],
  pages: 0,
  loading: false,
  filter: defaultFilter,
  columns: {
    annotations: true,
    primitives: true
  }
}

export default (state = initial, action) => {
  let filter = { ...state.filter }
  let columns = { ...state.columns }

  if (action.type === CONTRACTS.RECEIVED && action.payload.status !== 'OK') {
    console.warn('Server response is not OK', action.payload)
    return state
  }
  if (!action.payload) return state
  const { items, pages, total, filtered } = action.payload
  switch (action.type) {
  case CONTRACTS.FETCH:
    return { ...state, loading: true }
  case CONTRACTS.TURNPAGE:
    return { ...state, ...action.payload }
  case CONTRACTS.RECEIVED:
    return { ...state, loading: false, items, pages, total, filtered }
  case CONTRACTS.FILTER.TOGGLE:
    filter[action.payload] = !filter[action.payload]
    return {
      ...state,
      filter
    }
  case CONTRACTS.FILTER.APPLY:
    return {
      ...state,
      filter: { ...defaultFilter, ...filter, ...action.payload }
    }
  case CONTRACTS.FILTER.RESET:
    return {
      ...state,
      filter: { ...defaultFilter, ...filter, [action.payload.filter]: action.payload.value }
    }
  case CONTRACTS.COLUMN.TOGGLE:
    columns[action.payload] = !columns[action.payload]
    return {
      ...state,
      columns
    }

  default:
    return state
  }
}
