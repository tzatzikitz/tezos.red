import React, { PureComponent } from 'react'
import cytoscape from 'cytoscape'
import { object, func, array, number } from 'prop-types'
const { arraysEqual } = require('../common/utils')
let coseBilkent = require('cytoscape-cose-bilkent')
cytoscape.use(coseBilkent)

let layout = {
  name: 'breadthfirst',
  animate: false,
  ready: function() {},
  stop: function() {}
}

export const LAYOUTS = {
  breadthfirst: {
    circle: true,
    padding: 20
  },
  circle: {},
  'cose-bilkent' : {
    nodeDimensionsIncludeLabels: true,
    idealEdgeLength: 100
  }
}

let conf = {
  style: [
    {
      selector: 'node',
      style: {
        'background-color': 'data(color)',
        'text-outline-color': 'data(color)',
        'font-size': '24px',
        'text-valign': 'center',
        'text-outline-width': 2,
        'color': '#fff',
        'shape': 'data(shape)',
        'content': 'data(label)'
      }
    },
    {
      'selector': '.multiline-manual',
      'style': {
        'text-wrap': 'wrap'
      }
    },
    {
      selector: 'node.core',
      style: {
        'border-width': 4,
        'border-style': 'double',
        'border-color': 'data(color)'
      }
    },
    {
      selector: 'edge',
      style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle',
        'line-color': 'data(color)',
        'target-arrow-color': 'data(color)',
        'text-outline-color': 'data(color)'
      }
    },
    {
      selector: 'edge.delegate',
      style: {
        'line-style': 'dashed',
        'opacity': 0.5,
        'target-arrow-shape': 'diamond'
      }
    },
    {
      selector: 'edge.manager',
      style: {
        'line-style': 'dashed',
        'opacity': 0.8,
      }
    },
    {
      selector: 'edge.compound-delegate',
      style: {
        'line-style': 'dashed',
        'opacity': 0.5,
        'target-arrow-shape': 'diamond'
      }
    },
    {
      selector: 'edge.compound-transacts',
      style: {
        'source-arrow-shape': 'triangle',
        'source-arrow-color': '#567714',
        'target-arrow-shape': 'triangle'
      }
    }
  ],
  // pixelRatio: 1,
  textureOnViewport: true,
  motionBlur: true,
  layout
}

class Cytoscape extends PureComponent {
  cy = null;

  componentDidMount() {
    conf.container = this.container;
    conf.elements = []
    this.cy = cytoscape(conf)
    this.cy.on('tap', 'node', this.props.onNodeClick)
  }

  updateLayout() {
    let lay = {
      ...layout,
      ...LAYOUTS[this.props.options.layout],
      name: this.props.options.layout
    }
    this.layout = this.cy.layout(lay)
    this.layout.run()
  }

  compress({ threshold, shape, color, label, nodeSelector, edgeSelector, fn, cssClass, flipEdge } = {
    threshold: 10,
    shape: 'ellipse',
    flipEdge: false
  }) {
    let ns = this.cy.elements(nodeSelector)
    for (let i=0; i < ns.length; i++) {
      let node = ns[i]
      let id = node.id()
      let neighbours = node.connectedEdges(edgeSelector)
        .connectedNodes(fn)
      if (neighbours.length >= threshold) {
        neighbours.remove()
        this.cy.add({
          classes: cssClass,
          data: {
            id: `${id}-${cssClass}`,
            color,
            label: `${neighbours.length} ${label}`,
            shape
          }
        })
        this.cy.add({
          classes: cssClass,
          data: {
            id: `${id}-${cssClass}-edge`,
            color,
            source: flipEdge ? id : `${id}-${cssClass}`,
            target: flipEdge ? `${id}-${cssClass}` : id
          }
        })
      }
    }
  }

  componentDidUpdate() {
    const { elements } = this.props
    this.cy.startBatch()
    this.cy.json({ elements, layout: { name: 'null' } })
    this.compress({
      threshold: 10,
      shape: 'ellipse',
      color: '#567714',
      label: 'receivers',
      nodeSelector: 'node.sender',
      edgeSelector: '.sends',
      cssClass: 'compound-sender',
      fn: n => n._private.edges.length === 1,
      flipEdge: true
    })
    this.compress({
      threshold: 10,
      shape: 'ellipse',
      color: '#567714',
      label: 'transactors',
      nodeSelector: 'node.sender',
      edgeSelector: '.sends',
      fn: n => arraysEqual(
        n._private.edges.map(e => e._private.data.type),
        ['sends', 'sends']),
      cssClass: 'compound-transacts'
    })
    this.compress({
      threshold: 10,
      shape: 'rectangle',
      color: '#226666',
      label: 'delegees',
      nodeSelector: 'node.delegate',
      edgeSelector: '.delegate',
      fn: n => n._private.edges.length === 1,
      cssClass: 'compound-delegate'
    })
    let len = this.cy.elements().length
    console.log(`Graph after compression: ${len} elements.`)
    if (len >= 300) {
      console.warn(`Graph too large: ${len} elements. Render aborted.`)
    } else {
      this.updateLayout()
      this.cy.endBatch()
    }
  }

  componentWillUnmount() {
    this.cy.destroy();
  }

  setRef = (r) => {
    this.container = r
  }

  get height () {
    // 65px is the height of margin-top offset of navbar
    const h = this.props.height || (window.innerHeight - 65)
    return h + 'px'
  }

  get style () {
    return {
      width: '100%',
      height: this.height,
      display: 'block'
    }
  }

  render() {
    return <div style={this.style} ref={this.setRef} />
  }

  static propTypes = {
    elements: array,
    height: number,
    onNodeClick: func,
    options: object
  }

  static defaultProps = {
    elements: []
  }
}

export default Cytoscape
