import act from '../actions'
import React, { PureComponent } from 'react'
import CoreModal from './CoreModal'
import { Link } from 'react-router-dom'
import { ButtonGroup, Button, Glyphicon } from 'react-bootstrap'
import Balance from '../common/Balance'
const { object, func, arrayOf, string } = require('prop-types')

class Controls extends PureComponent {
  setRef = (m) => {
    this.coremodal = m
  }

  showCore = () => {
    this.coremodal.getWrappedInstance().handleShow()
  }

  toggleLayout = () => {
    this.props.dispatch(act.graph.options.toggleLayout())
  }

  addToCore = (contract = this.props.nodeId) => {
    this.props.dispatch(act.graph.core.add(contract))
  }

  removeFromCore = (contract) => {
    this.props.dispatch(act.graph.core.remove(contract))
  }

  render() {
    const { core, nodeId, contract, nodeData } = this.props
    const { balance } = nodeData
    return <div className="controls-outer">
      <div className="controls-inner">
        <Link to={`/${nodeId}`}>
          {this.props.nodeId}
        </Link>
        {balance && <span>
        &nbsp;has <Balance balance={balance} />
        </span>}
        <ButtonGroup className="pull-right">
          {core.includes(nodeId) ?
            <Button bsSize="small" title="Remove from core"
              onClick={() => this.removeFromCore(nodeId)}
              disabled={!nodeId || nodeId === contract} >
              <Glyphicon glyph="minus" /></Button> :
            <Button bsSize="small" title="Add to core"
              onClick={() => this.addToCore(nodeId)} disabled={!nodeId} >
              <Glyphicon glyph="plus" /></Button>}
          <Button bsSize="small" title="Toggle layout"
            onClick={() => this.toggleLayout()} >
            <Glyphicon glyph="eye-open" /></Button>
          {/* <Button bsSize="small" title="Copy to clipboard" disabled={!nodeId} >
              <Glyphicon glyph="copy" /></Button> */}
          <Button bsSize="small" title="View core" onClick={() => this.showCore()}>
            <Glyphicon glyph="list" /></Button>
        </ButtonGroup>
      </div>
      <CoreModal
        contract={contract}
        ref={this.setRef}
        rm={this.removeFromCore}
        add={this.addToCore}
        core={this.props.core} />
    </div>
  }

  static propTypes = {
    nodeId: string,
    dispatch: func,
    contract: string,
    core: arrayOf(string),
    nodeData: object
  }
}

export default Controls
