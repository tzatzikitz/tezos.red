import React, { Component } from 'react'
import { ButtonGroup, FormGroup, FormControl, Table, Button, Glyphicon } from 'react-bootstrap'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import './utils.css'
const { object, func, arrayOf, string } = require('prop-types')


class Collections extends Component {
  constructor() {
    super()
    this.state = {
      name: ''
    }
  }

  handleChange = (e) => {
    this.setState({ name: e.target.value });
  }

  save = () => {
    this.props.handleSave(this.state.name, this.props.core)
    this.setState({ name: '' });
  }

  validate() {
    return this.formValid ? 'success' : 'error'
  }

  get formValid() {
    let { name } = this.state
    if (!name || !name.length) return false
    if (this.props.collections.filter(c => c.name === name).length) return false
    return true
  }

  keyPress = (e) => {
    if (e.keyCode === 13 && this.formValid) this.save()
  }

  load = (name) => {
    this.props.handleLoad(name)
  }

  linkText = (name) => {
    const collection = this.props.collections.find(c => c.name === name)
    const root = process.env.REACT_APP_ROOT
    return `${root}/graph/${collection.nodes.join(',')}`
  }

  render() {
    const { core, collections } = this.props
    return (
      <Table condensed hover style={{ marginBottom: 0 }}>
        <thead>
          <tr>
            <th>Collection</th>
            <th>Contracts</th>
            <th style={{ width: '70px' }}></th>
          </tr>
        </thead>
        <tbody>
          {collections.map(({ name, nodes = [] }, i) => <tr key={i}>
            <td>
              <Button className="link" bsStyle="link" bsSize="small" onClick={() => this.load(name)}>{name}</Button>
            </td>
            <td>{nodes.length}</td>
            <td>
              <ButtonGroup>
                <CopyToClipboard text={this.linkText(name)}>
                  <Button bsSize="xsmall" title="Copy shareable link to clipboard">
                    <Glyphicon glyph="copy" />
                  </Button>
                </CopyToClipboard>
                <Button bsSize="xsmall" title="Delete" onClick={() => this.props.handleRemove(name)}>
                  <Glyphicon glyph="remove-sign" />
                </Button>
              </ButtonGroup>
            </td>
          </tr>)}
          <tr>
            <td>
              <FormGroup bsSize='small' style={{ marginBottom: 0 }} validationState={this.validate()}>
                <FormControl
                  type="text"
                  value={this.state.name}
                  placeholder={`Save collection of ${core.length} contracts as...`}
                  onKeyDown={this.keyPress}
                  onChange={this.handleChange} />
              </FormGroup>
            </td>
            <td></td>
            <td style={{ verticalAlign: 'middle' }}>
              <Button bsSize="xsmall" title="Save"
                disabled={!this.formValid}
                onClick={this.save}>
                <Glyphicon glyph="floppy-disk" />
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    )
  }
  static propTypes = {
    core: arrayOf(string),
    collections: arrayOf(object),
    handleSave: func.isRequired,
    handleLoad: func.isRequired,
    handleRemove: func.isRequired
  }
  static defaultProps = {
    core: [],
    collections: []
  }
}

export default Collections
