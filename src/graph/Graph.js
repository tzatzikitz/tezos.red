import React, { Component } from 'react'
import Cytoscape from './cytoscape'
import { connect } from 'react-redux'
import act from '../actions'
import './Controls.css'
import Controls from './controls'
const { object, func, arrayOf, array, string, number } = require('prop-types')
const { uniq, arraysEqual } = require('../common/utils')


class Graph extends Component {
  constructor() {
    super()
    this.state = {
      nodeData: {}
    }
  }

  core (props = this.props) {
    // start with core from the store
    let result = [ ...props.stateCore ] || []

    // add contract (used in the details page)
    const { contract, urlCore } = props
    if (contract && !result.includes(contract)) result.push(contract)

    // add url core
    result = result.concat(urlCore)

    // remove duplicates
    return uniq(result.filter(x => x && x.length))
  }

  componentDidMount() {
    this.props.dispatch(act.graph.fetch(this.core()))
  }

  componentDidUpdate(prevProps) {
    const c = this.core()
    const pc = this.core(prevProps)
    if (!arraysEqual(pc, c)) {
      this.props.dispatch(act.graph.fetch(c))
    }
  }

  nodeClick = (e) => {
    // TODO allow for selecting of compound node elements
    let id = e.target.id()
    if (id.search(/-compound-/) !== -1) id = undefined
    this.setState({ nodeId: id, nodeData: e.target.data() })
  }

  render() {
    const { elements, height, options } = this.props
    const core = this.core()
    return (<div>
      <Controls
        contract={this.props.contract}
        nodeId={this.state.nodeId}
        nodeData={this.state.nodeData}
        core={core}
        dispatch={this.props.dispatch} />
      <Cytoscape
        onNodeClick={this.nodeClick}
        height={height}
        options={options}
        elements={elements} />
    </div>)
  }

  static propTypes = {
    match: object,
    dispatch: func,
    elements: array,
    urlCore: arrayOf(string),
    stateCore: arrayOf(string),
    options: object,
    contract: string,
    height: number
  }
  static defaultProps = {
    elements: []
  }
}

export default connect(state => ({
  elements: state.graph.elements,
  options: state.graph.options,
  stateCore: state.graph.core
}))(Graph)
