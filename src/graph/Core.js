import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { FormGroup, FormControl, Table, Button, Glyphicon } from 'react-bootstrap'
const { func, arrayOf, string } = require('prop-types')


class Core extends Component {
  constructor() {
    super()
    this.state = {
      newContract: ''
    }
  }

  handleChange = (e) => {
    this.setState({ newContract: e.target.value });
  }

  add = () => {
    this.props.handleAdd(this.state.newContract)
    this.setState({ newContract: '' });
  }

  get formValid() {
    return !!this.state.newContract.match(/^(tz\d|KT1)/)
  }

  validate() {
    return this.formValid ? 'success' : 'error'
  }

  keyPress = (e) => {
    if (e.keyCode === 13 && this.formValid) this.add()
  }

  render() {
    const { core, nodes } = this.props
    return (
      <Table condensed hover style={{ marginBottom: 0 }}>
        <thead>
          <tr>
            <th>Contract</th>
            <th style={{ width: '40px' }}></th>
          </tr>
        </thead>
        <tbody>
          {core.map((contract, i) => <tr key={i}>
            <td> <Link to={`/${contract}`}> { contract }</Link> </td>
            <td>
              <Button bsSize="xsmall" title="Remove"
                disabled={this.props.contract === contract}
                onClick={() => this.props.handleRemove(contract)} >
                <Glyphicon glyph="minus" />
              </Button>
            </td>
          </tr>)}
          {nodes.filter(x => !core.includes(x)).map((contract, i) => <tr key={`n${i}`}>
            <td> <Link to={`/${contract}`}> { contract }</Link> </td>
            <td>
              <Button bsSize="xsmall" title="Add"
                onClick={() => this.props.handleAdd(contract)} >
                <Glyphicon glyph="plus" />
              </Button>
            </td>
          </tr>)}
          <tr>
            <td>
              <FormGroup bsSize='small' style={{ marginBottom: 0 }} validationState={this.validate()}>
                <FormControl
                  type="text"
                  value={this.state.newContract}
                  placeholder="Add contract..."
                  onKeyDown={this.keyPress}
                  onChange={this.handleChange} />
              </FormGroup>
            </td>
            <td style={{ verticalAlign: 'middle' }}>
              <Button bsSize="xsmall" title="Add" onClick={this.add}
                disabled={!this.formValid} >
                <Glyphicon glyph="plus" />
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    )
  }
  static propTypes = {
    core: arrayOf(string),
    nodes: arrayOf(string),
    handleAdd: func.isRequired,
    contract: string,
    handleRemove: func.isRequired
  }
  static defaultProps = {
    core: [],
    nodes: []
  }
}

export default Core
