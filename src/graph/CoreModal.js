import React, { Component } from 'react'
import { connect } from 'react-redux'
import act from '../actions'
import { Modal, Button } from 'react-bootstrap'
import Collections from './Collections'
import Core from './Core'
const { object, func, arrayOf, string } = require('prop-types')


class CoreModal extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      show: false
    }
  }

  handleClose = () => {
    this.setState({ show: false })
  }

  handleShow = () => {
    this.setState({ show: true })
  }

  render() {
    const { core, nodes, collections, contract } = this.props
    return (
      <Modal bsSize="large" show={this.state.show} onHide={this.handleClose}>
        <Modal.Body>
          <Collections
            core={core} collections={collections}
            handleSave={this.props.saveCollection}
            handleRemove={this.props.removeCollection}
            handleLoad={this.props.loadCollection} />
          <Core
            core={core} nodes={nodes} contract={contract}
            handleAdd={this.props.addToCore}
            handleRemove={this.props.removeFromCore} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }
  static propTypes = {
    core: arrayOf(string),
    contract: string,
    nodes: arrayOf(string),
    collections: arrayOf(object),
    removeFromCore: func.isRequired,
    addToCore: func.isRequired,
    saveCollection: func.isRequired,
    loadCollection: func.isRequired,
    removeCollection: func.isRequired
  }
  static defaultProps = {
    nodes: [],
    collections: []
  }
}

export default connect(state => ({
  nodes: state.graph.nodes,
  collections: state.graph.collections
}), dispatch => ({
  removeFromCore: (contract) => dispatch(act.graph.core.remove(contract)),
  addToCore: (contract) => dispatch(act.graph.core.add(contract)),
  saveCollection: (name, nodes) => dispatch(act.graph.collection.save(name, nodes)),
  removeCollection: (name) => dispatch(act.graph.collection.remove(name)),
  loadCollection: (name) => dispatch(act.graph.collection.load(name))
}), null, { withRef: true })(CoreModal)
