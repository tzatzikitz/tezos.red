import React, { Component } from 'react'
import { Panel, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Graph from './Graph'
import act from '../actions'
import { func, bool, string } from 'prop-types'

class DetailsGraph extends Component {
  togglePanel = () => {
    this.props.togglePanel()
  }
  render() {
    const { contract, show } = this.props
    return <Panel>
      <Panel.Heading onClick={this.togglePanel}>
        <Panel.Title componentClass="h3">Graph
          <Link to={'/graph/' + contract} title="View Fullscreen Graph" className="pull-right">
            <Glyphicon glyph="fullscreen" /></Link>
        </Panel.Title>
      </Panel.Heading>
      {show && <Panel.Body style={{ padding: 0 }}>
        <Graph contract={contract} height={450} />
      </Panel.Body>}
    </Panel>
  }
  static propTypes = {
    contract: string,
    togglePanel: func,
    show: bool
  }
  static defaultProps = {
    show: true
  }
}

export default connect(
  state => ({
    show: state.graph.show
  }), dispatch => ({
    togglePanel: () => dispatch(act.graph.togglePanel())
  }))(DetailsGraph)
