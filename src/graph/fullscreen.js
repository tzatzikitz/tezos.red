import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Grid, Panel } from 'react-bootstrap'
import Graph from './Graph.js'
import { Link } from 'react-router-dom'
const { object, arrayOf, string } = require('prop-types')
const { uniq } = require('../common/utils')

const foundationBakers = [
  'tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9',
  'tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5',
  'tz3bTdwZinP8U1JmSweNzVKhmwafqWmFWRfk',
  'tz3NExpXn9aPNZPorRE4SdjJ2RGrfbJgMAaV',
  'tz3UoffC7FG7zfpmvmjUmUeAaHvzdcUvAj6r',
  'tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K',
  'tz3VEZ4k6a4Wx42iyev6i2aVAptTRLEAivNN',
  'tz3RB4aoyjov4KEVRbuhvQ1CKJgBJMWhaeB8',
  'tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU'
]

class GraphFullscreen extends PureComponent {
  core (props = this.props) {
    let result = [ ...props.core ] || []
    let { core = '' } = props.match.params
    core = core.split(',')
    result = result.concat(core)
    return uniq(result.filter(x => x && x.length))
  }

  get height () {
    return window.innerHeight - 65 - 22
  }

  render() {
    const core = this.core()
    if (!core.length) return (
      <Grid style={{ marginTop: '65px' }}>
        <Panel>
          <Panel.Heading>
            Graph
          </Panel.Heading>
          <Panel.Body>
            <p>
              The graph is empty.
              Visit a contract, and use the fullscreen button to display the graph here. This will set the <i>core</i> to that contract.
            </p>
            <p>
              Alternatively, use the collections dialog (found on the contract details page) to load the core of the graph, if you have previously saved a collection.
            </p>
            <p>
              You can also visit <b>/graph/<i>contract1,contract2,...</i></b> for the same effect.
            </p>
            <p>
              For a sample, check out this graph of <Link to={`/graph/${foundationBakers.join(',')}`}>Foundation Bakers</Link>.
            </p>
          </Panel.Body>
        </Panel>
      </Grid>)
    return <Graph height={this.height} urlCore={core} />
  }

  static propTypes = {
    match: object,
    core: arrayOf(string)
  }
}

export default connect(state => ({
  core: state.graph.core
}))(GraphFullscreen)
