import React, { Component } from 'react'
import Primitive from './tree/Primitive'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import act from './actions'
import { func, string, any, object } from 'prop-types'
import RelativeDate from './common/relative-date'
const { Panel, Table: BsTable, Grid } = require('react-bootstrap')

const LegendRow = ({ type, example }) => {
  example = example || type
  return (
    <tr>
      <td>{type}</td>
      <td>
        <Primitive type={type}>{example}</Primitive>
        <Primitive type={type} />
      </td>
    </tr>
  )
}
LegendRow.propTypes = {
  type: string,
  example: any
}

class About extends Component {
  componentDidMount() {
    this.props.dispatch(act.meta.fetch())
  }

  render() {
    const { lastBlock } = this.props.meta
    return (
      <Grid style={{ marginTop: '65px' }}>
        <Panel>
          <Panel.Heading>
            About
          </Panel.Heading>
          <Panel.Body>
            <p>
              Tezos.red is a collection of tools for exploring and interacting
              with the Tezos blockchain.
            </p>
            <p>
              We are a community project and seek to
              expand on the features of <i>tzscan.io</i>, not to duplicate it.
            </p>
            <p>
              The <strong>mainnet</strong> has been used to extract
              the contract data <RelativeDate dotted date={lastBlock} />.
            </p>
          </Panel.Body>
        </Panel>
        <Panel>
          <Panel.Heading>
            Contact
          </Panel.Heading>
          <Panel.Body>
            <p>
              Twitter: <a href="https://twitter.com/tzatzikitz">@tzatzikitz</a>
            </p>
            <p>
              Email: contact@...
            </p>
            <p>
              Riot chat: <a href="https://riot.im/app/#/room/#tezos.red:matrix.org">#tezos.red:matrix.org</a>
            </p>
            <p>
              Support the project: <Link to="/tz1RED6k6W4SuiJJ69urRQXbP6vPR775wsAc">tz1RED6k6W4SuiJJ69urRQXbP6vPR775wsAc</Link>
            </p>
          </Panel.Body>
        </Panel>
        <Panel>
          <Panel.Heading>
            Help, Definitions and Legend
          </Panel.Heading>
          <Panel.Body>
            <p>
              <i>Code size</i>, the complexity of the contract, is the number of
              Michelson primitives used in the code.
            </p>
            <p>
              <i>Promiscuity</i> is the number of unique addresses that a
              contract has transacted with.
            </p>
            <p>
              <i>Graph core</i> is a set of contracts. These, together with
              related contracts, are the nodes of the graph. Contracts are
              realted by their relationships - tansacting, managing, delegating
              - these are the edges of the graph. You can save a core into a
              <i> collection</i>. This aids in analysis of different parts of the
              network.
            </p>

            The following legend defines the colors used to represent the nested Michelson smart contract primitives:
            <BsTable striped bordered condensed hover>
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Examples</th>
                </tr>
              </thead>
              <tbody>
                <LegendRow type="pair" />
                <LegendRow type="string" example="hello" />
                <LegendRow type="int" example="42" />
                <LegendRow type="empty" example="[]" />
                <LegendRow type="some" />
              </tbody>
            </BsTable>
          </Panel.Body>
        </Panel>
      </Grid>
    )
  }
}

About.propTypes = {
  dispatch: func,
  meta: object
}

export default connect(state => ({
  meta: state.meta.data
}))(About)
