const { keys, values } = Object
const { isArray } = Array

function toArray(obj) {
  return isArray(obj) ? obj : [obj]
}

function isPA(obj) {
  // return obj.prim && obj.args
  return obj.prim
}

function isLiteral(obj) {
  return !isArray(obj) && !obj.prim && !obj.args && (keys(obj).length === 1)
}

function literalType(obj) {
  return isLiteral(obj) && keys(obj)[0]
}

function literalValue(obj) {
  return isLiteral(obj) && values(obj)[0]
}

function empty(obj) {
  if (!obj) return true
  return isArray(obj) && (obj.length === 0)
}

module.exports = {
  isArray, toArray, empty, isPA, isLiteral, literalType, literalValue
}
