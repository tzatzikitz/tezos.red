import React, { PureComponent } from 'react'
import './Tree.css'
import { toArray } from './utils'
import Primitive from './Primitive'
import { empty, isPA, isArray, isLiteral, literalType, literalValue } from './utils'
const { array, object, oneOfType } = require('prop-types')


class Tree extends PureComponent {
  get objs() {
    return toArray(this.props.obj)
  }

  render() {
    const { options } = this.props
    const { objs } = this

    if (empty(objs)) return (
      <Primitive type={'EMPTY'}>[]</Primitive>)

    return objs.map(
      (obj, idx) => {
        if(isLiteral(obj)) {
          return (
            <Primitive key={idx} type={literalType(obj)}>
              {literalValue(obj)}
            </Primitive>)
        } else if(isPA(obj)) {
          return (
            <Primitive key={idx} type={obj.prim} annots={obj.annots}>
              {(options.renderEmptyArgs || !empty(obj.args)) &&
               <Tree obj={obj.args} options={options} />}
            </Primitive>)
        } else if(isArray(obj)) {
          return <Tree key={idx} obj={obj} options={options} />
        } else {
          return obj
        }
      }
    )
  }

  static propTypes = {
    obj: oneOfType([array, object]),
    options: object
  }
  static defaultProps = {
    obj: [],
    options: {
      renderEmptyArgs: false
    }
  }
}

export default Tree
