import './Primitive.css'
import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import { primStyle, hasColor } from './colors'
const { any, string, arrayOf } = require('prop-types')


const Primitive = ({ annots, type, children }) => {
  type = type && type.toUpperCase()
  const hasLabel = !hasColor(type)
  const value = (!hasLabel && !children) ? '\u00A0' : children
  let style = primStyle(type)
  if (annots) {
    style.borderStyle = 'dashed'
  }
  const result = (
    <span style={style} className={'prim'} >
      {!hasColor(type) && <label>{type}</label>} {value}
    </span>)
  if (annots) {
    const tooltip = <Tooltip id="tooltip">{annots.join(', ')}</Tooltip>
    return <OverlayTrigger placement="top" overlay={tooltip}>{result}</OverlayTrigger>
  } else {
    return result
  }
}
Primitive.propTypes = {
  type: string,
  children: any,
  annots: arrayOf(string)
}

export default Primitive
