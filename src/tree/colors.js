const tinycolor = require('tinycolor2');

function darken(color) {
  return tinycolor(color).darken(10).toString()
}

const backgroundColors = {
  EMPTY: '#000',
  INT: '#449',
  NONE: '#333',
  PAIR: '#fef',
  SOME: 'yellow',
  STRING: '#494',
}

const foregroundColors = {
  SOME: '#333',
  STRING: '#eee',
  UNIT: '#eee'
}

function hasColor(type) {
  return !!backgroundColors[type]
}

function primStyle(type) {
  const bg = backgroundColors[type] || '#fff'
  const fg = foregroundColors[type] || '#ddd'
  return {
    backgroundColor: bg,
    borderColor: darken(bg),
    color: fg
  }
}

module.exports = {
  primStyle, hasColor
}
