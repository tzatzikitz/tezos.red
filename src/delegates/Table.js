import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactTable from 'react-table'
import act from '../actions'
import { Link } from 'react-router-dom'
import Balance from '../common/Balance'
import Ratio from '../common/ratio'
const { Grid } = require('react-bootstrap')
const { array, bool, func, number } = require('prop-types')


class Table extends Component {
  get columns() {
    return [
      {
        Header: 'Delegate',
        columns: [
          {
            accessor: 'address',
            Header: 'Address',
            minWidth: 230,
            Cell: row => <Link to={`/${row.value}`}>{row.value}</Link>,
          },
          {
            Header: 'Name',
            accessor: 'name',
          }
        ]
      },
      {
        Header: 'Clients',
        minWidth: 90,
        columns: [
          {
            Header: 'Contracts',
            accessor: 'accounts',
            minWidth: 30
          },
          {
            Header: 'Identities',
            accessor: 'clients',
            minWidth: 30
          },
          {
            Header: 'Ratio',
            accessor: 'ratio',
            Cell: row => <Ratio ratio={row.value} />,
            minWidth: 30
          }
        ]
      },
      {
        Header: 'Staking Balance',
        accessor: 'stake',
        Cell: row => <Balance balance={row.value} />
      },
      {
        Header: 'Rolls',
        accessor: 'stake',
        Cell: row => Math.floor(row.value / 10000000000),
      }
    ]
  }

  fetchData = (state) => {
    this.props.fetchDelegates(
      state.pageSize,
      state.page,
      state.sorted,
      state.filtered)
  }

  render() {
    const { loading, pages } = this.props
    return (
      <Grid style={{marginTop: '65px'}} fluid>
        <ReactTable
          manual
          onFetchData={this.fetchData}
          multiSort={false}
          data={this.props.delegates}
          columns={this.columns}
          loading={loading}
          pages={pages}
          pageSizeOptions={[5, 10, 15, 20, 25, 50, 100]}
          defaultPageSize={20}
          defaultSorted={[{ id: 'stake', desc: true }]}
          showPaginationTop
          showPaginationBottom={false}
          className="-striped -highlight" />
      </Grid>
    )
  }

  static propTypes = {
    delegates: array,
    fetchDelegates: func,
    loading: bool,
    pages: number,
  }
}

const mapStateToProps = state => ({
  delegates: state.delegates.items,
  pages: state.delegates.pages,
  loading: state.delegates.loading
})

const mapDispatchToProps = dispatch => ({
  fetchDelegates: (...params) => dispatch(act.delegates.fetch(...params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Table)
