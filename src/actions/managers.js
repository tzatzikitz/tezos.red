const fetch = require('cross-fetch')
const { MANAGERS } = require('../constants')

const received = (payload) => ({ type: MANAGERS.RECEIVED, payload })

function shouldFetch(state) {
  const { loading } = state.managers
  return !loading
}

function fetchManagers(pageSize, page, sorted) {
  // TODO accept `filtered` as fourth argument
  const url = new URL(process.env.REACT_APP_API + '/managers')
  url.search = new URLSearchParams({
    page,
    limit: pageSize,
    orderby: sorted[0].id,
    orderdir: sorted[0].desc ? 'desc': 'asc'
  })
  return (dispatch, getState) => {
    if(!shouldFetch(getState())) return Promise.resolve()
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

module.exports = {
  fetch: fetchManagers,
}
