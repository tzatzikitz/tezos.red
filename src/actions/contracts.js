const fetch = require('cross-fetch')
const { CONTRACTS } = require('../constants')
const { flatten } = require('../common/utils')

const received = (payload) => ({ type: CONTRACTS.RECEIVED, payload })

function fetchContracts() {
  return (dispatch, getState) => {

    const { contracts } = getState()
    let { loading, page, pageSize, sorted, filter } = contracts

    const url = new URL(process.env.REACT_APP_API + '/contracts')
    url.search = new URLSearchParams({
      page,
      limit: pageSize,
      orderby: sorted[0].id,
      orderdir: sorted[0].desc ? 'desc': 'asc',
      ...flatten({ filter }, [null, undefined])
    })

    if(loading) return Promise.resolve()
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

const toggleFilter = (filter) => ({
  type: CONTRACTS.FILTER.TOGGLE,
  payload: filter
})

const resetFilter = (filter, value = null) => ({
  type: CONTRACTS.FILTER.RESET,
  payload: { filter, value }
})

const applyFilter = (filter) => ({
  type: CONTRACTS.FILTER.APPLY,
  payload: filter
})

const toggleColumn = (column) => ({
  type: CONTRACTS.COLUMN.TOGGLE,
  payload: column
})

const turnPage = (page, pageSize, sorted) => ({
  type: CONTRACTS.TURNPAGE,
  payload: { page, pageSize, sorted }
})

module.exports = {
  fetch: fetchContracts,
  applyFilter,
  turnPage,
  toggleFilter,
  resetFilter,
  toggleColumn
}
