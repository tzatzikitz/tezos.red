const fetch = require('cross-fetch')
const { SIMILARITY } = require('../constants')

const received = (payload) => ({ type: SIMILARITY.RECEIVED, payload })

function shouldFetch(state) {
  const { similarity } = state
  return !similarity.length
}

function fetchSimilarity() {
  const url = new URL(process.env.REACT_APP_API + '/similarity')
  return (dispatch, getState) => {
    if(!shouldFetch(getState())) return Promise.resolve()
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

function zoom(payload) {
  return { type: SIMILARITY.ZOOM, payload }
}

module.exports = {
  fetch: fetchSimilarity,
  zoom
}
