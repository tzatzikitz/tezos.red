const fetch = require('cross-fetch')
const { DETAILS } = require('../constants')


function shouldFetch(state, contract) {
  const { loading } = state.details
  return !state.details[contract] && !loading
}

function fetchDetails(contract) {
  const received = (payload) => ({ type: DETAILS.RECEIVED.DETAILS, payload })
  const loading = () => ({ type: DETAILS.FETCH })

  return (dispatch, getState) => {
    if(!shouldFetch(getState(), contract)) return Promise.resolve()
    dispatch(loading())
    const url = new URL(process.env.REACT_APP_API + '/contract/' + contract)
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

function fetchTransactions(contract, pageSize, page, sorted) {
  const received = (payload) => ({ type: DETAILS.RECEIVED.TRANSACTIONS, payload })
  const url = new URL(process.env.REACT_APP_API + '/transactions/' + contract)
  url.search = new URLSearchParams({
    page,
    limit: pageSize,
    orderby: sorted[0].id,
    orderdir: sorted[0].desc ? 'desc': 'asc'
  })
  return (dispatch) => {
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

module.exports = {
  fetch: fetchDetails,
  fetchTransactions
}

