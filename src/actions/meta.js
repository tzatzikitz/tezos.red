const fetch = require('cross-fetch')
const { META } = require('../constants')

const received = (payload) => ({ type: META.RECEIVED, payload })

function shouldFetch(state) {
  const { data, loading } = state.meta
  return !Object.keys(data).length && !loading
}

function fetchMeta() {
  return (dispatch, getState) => {
    if(!shouldFetch(getState())) return Promise.resolve()
    return fetch(process.env.REACT_APP_API + '/meta')
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

module.exports = {
  fetch: fetchMeta
}

