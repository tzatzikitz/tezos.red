const fetch = require('cross-fetch')
const { GRAPH } = require('../constants')

const received = (payload) => ({ type: GRAPH.RECEIVED, payload })

function shouldFetch(state) {
  const { loading } = state.graph
  return !loading
}

function fetchGraph(core = []) {
  const url = new URL(process.env.REACT_APP_API + '/graph')
  url.search = new URLSearchParams({
    core: core.join(',')
  })
  return (dispatch, getState) => {
    if(!shouldFetch(getState())) return Promise.resolve()
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        dispatch(received(json))
        return json
      })
  }
}

module.exports = {
  fetch: fetchGraph,
  togglePanel: () => {
    return (dispatch) => {
      dispatch({ type: GRAPH.TOGGLEPANEL })
    }
  },
  core: {
    add: (node) => {
      return (dispatch) => {
        dispatch({ type: GRAPH.CORE.ADD, payload: node })
      }
    },
    remove: (node) => {
      return (dispatch) => {
        dispatch({ type: GRAPH.CORE.REMOVE, payload: node })
      }
    }
  },
  options: {
    toggleLayout: () => {
      return (dispatch) => {
        dispatch({ type: GRAPH.OPTIONS.TOGGLELAYOUT })
      }
    }
  },
  collection: {
    save: (name, nodes) => {
      return (dispatch) => {
        dispatch({ type: GRAPH.COLLECTION.SAVE, payload: { name, nodes } })
      }
    },
    load: (name) => {
      return (dispatch) => {
        dispatch({ type: GRAPH.COLLECTION.LOAD, payload: name })
      }
    },
    remove: (name) => {
      return (dispatch) => {
        dispatch({ type: GRAPH.COLLECTION.REMOVE, payload: name })
      }
    }
  }
}
