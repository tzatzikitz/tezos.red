const { expect } = require('chai')
const { size, walk } = require('./size')

it('only works for arrays', () => {
  expect(size()).to.eql(0)
  expect(size({})).to.eql(0)
  expect(size([])).to.eql(0)
})

it('simple contract', () => {
  expect(size([
    {
      'prim': 'CDR',
      'args': []
    },
    {
      'prim': 'UNIT',
      'args': []
    },
    {
      'prim': 'PAIR',
      'args': []
    }
  ])).to.eql(3)
})

it('simple contract', () => {
  expect(size([
    {
      'prim': 'DUP',
      'args': []
    },
    {
      'prim': 'CAR',
      'args': []
    },
    {
      'prim': 'DIP',
      'args': [
        [
          {
            'prim': 'CDR',
            'args': []
          },
          {
            'prim': 'DUP',
            'args': []
          }
        ]
      ]
    },
    {
      'prim': 'SWAP',
      'args': []
    },
    {
      'prim': 'CONCAT',
      'args': []
    },
    {
      'prim': 'PAIR',
      'args': []
    }
  ])).to.eql(8)
})

it('walks', () => {
  const storage = {
    'prim': 'Pair',
    'args': [
      [
        {
          'prim': 'Elt',
          'args': [
            {
              'string': 'one'
            },
            {
              'string': 'two'
            }
          ]
        }
      ],
      {
        'string': 'three'
      }
    ]
  }
  let coll = []
  walk([storage], obj => coll.push(obj))
  expect(coll.length).to.eql(3)
})
