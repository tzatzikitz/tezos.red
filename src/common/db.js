const { Client } = require('pg')
const Promise = require('bluebird')
var client

async function updateContractsTS(contract, value) {
  const query = `UPDATE contracts SET ${this} = $1::timestamp WHERE id = $2`
  return await client.query(query, [value, contract])
}

/**
 * Wrap one of the db functions in this to report errors without uninformative
 * Promise rejection errors
 */
function wrapper(fn) {
  return (...args) => {
    try {
      return fn(...args)
    } catch (err) {
      console.error(err)
    }
  }
}

const writeContract = (details) => {
  const { contract, balance, counter, delegate, manager,
    codeSize, annotations, primitives, script, spendable } = details
  delete details.contract
  delete details.balance
  delete details.counter
  delete details.delegate
  delete details.manager
  delete details.codeSize
  delete details.annotations
  delete details.primitives
  delete details.script
  delete details.spendable
  return client.query(`
INSERT INTO contracts(id, json, balance, counter, delegate, manager,
codesize, annotations, primitives, script, spendable)
VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
ON CONFLICT(id) DO UPDATE SET
json = excluded.json,
balance = excluded.balance,
counter = excluded.counter,
delegate = excluded.delegate,
manager = excluded.manager,
codesize = excluded.codesize,
annotations = excluded.annotations,
primitives = excluded.primitives,
script = excluded.script,
spendable = excluded.spendable
`, [
    contract, details, balance, counter, delegate.value, manager,
    codeSize, annotations, primitives, script, spendable
  ])
}

// remove unicode NULL, otherwise postgres throws and error
function deepTransform(obj) {
  if (obj && typeof obj === 'object') {
    var allKeys = Object.keys(obj)
    for(var i = 0 ; i < allKeys.length ; i++){
      var k = allKeys[i]
      var value = obj[k]
      if ( typeof value === 'string') {
        obj[k] = value.replace('\u0000', '')
      } else if ( typeof value === 'object') {
        deepTransform(value)
      }
    }
  }
  return obj
}

module.exports = {
  connect: async () => {
    client = new Client({
      database: 'tezos-red'
    })
    await client.connect()
    return client
  },
  end: async () => { await client.end() },
  block: {
    write: async (block) => {
      return await client.query('INSERT INTO blocks(id, predecessor, ts, json) VALUES($1, $2, $3, $4) ON CONFLICT DO NOTHING', [
        block.hash, block.header.predecessor, block.header.timestamp, deepTransform(block)
      ])
    },
    get: async (hash) => {
      let res = await client.query('SELECT json FROM blocks WHERE id = $1::text', [hash])
      if(!res.rows.length) return
      return res.rows[0].json
    }
  },
  contract: {
    all: async () => {
      // TODO filter with code only (maybe not, only for api?)
      return (await client.query('SELECT * FROM contracts')).rows
    },
    clearOthers: contracts => {
      return client.query('DELETE FROM contracts WHERE id NOT IN (\'' + contracts.join('\',\'') + '\')')
    },
    write: wrapper(writeContract),
    activatedOn: updateContractsTS.bind('activated'),
    originatedOn: updateContractsTS.bind('originated')
  },
  transaction: {
    add: (ophash, idx, ts, obj) => {
      const { amount, kind, source, destination, counter } = obj
      const status = obj.metadata.operation_result && obj.metadata.operation_result.status
      if (status && status !== 'applied') return
      delete obj.amount
      delete obj.kind
      delete obj.source
      delete obj.destination
      delete obj.counter
      return client.query('INSERT INTO transactions(ophash, idx, ts, kind, amount, src, dst, counter, json) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT DO NOTHING', [
        ophash, idx, ts, kind, amount, source, destination, counter, obj
      ])
    },
    all: async () => {
      return (await client.query('SELECT ts, src, dst, amount FROM transactions WHERE kind = "transaction" ORDER BY ts DESC')).rows
    }
  },
  similarity: {
    writeAll: (tsne) => {
      return Promise.map(tsne, ({ contract, x, y, size }) => {
        return client.query(`INSERT INTO similarity(contract, x, y, size)
VALUES($1, $2, $3, $4) ON CONFLICT(contract) DO UPDATE SET
x = excluded.x,
y = excluded.y,
size = excluded.size
`, [ contract, x, y, size ])
      })
    }
  },
  delegate: {
    insert: async (d) => {
      return await client.query(`INSERT INTO delegates(name, address, stake)
VALUES($1, $2, $3) ON CONFLICT(address) DO UPDATE SET
name = COALESCE(excluded.name, delegates.name),
stake = COALESCE(excluded.stake, delegates.stake)
`, [d.name, d.address, d.stake])
    }
  }
}
