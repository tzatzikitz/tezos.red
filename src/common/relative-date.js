import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import Moment from 'react-moment'
const { string, bool } = require('prop-types')

const RelativeDate = ({ date, dotted }) => {
  if (!date) return null
  const tooltip = (
    <Tooltip id="tooltip">
      {new Date(date).toLocaleString()}
    </Tooltip>)
  const style = dotted ? { borderBottom: '1px dotted' } : {}
  return (
    <OverlayTrigger placement="bottom" overlay={tooltip}>
      <Moment style={style} fromNow date={date}/>
    </OverlayTrigger>)
}

RelativeDate.propTypes = {
  date: string,
  dotted: bool
}

export default RelativeDate
