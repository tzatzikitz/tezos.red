import React from 'react'
import Balance from './Balance'

function helper(accessor, header=accessor) {
  return {
    accessor: `volume.${accessor}`,
    Header: header,
    Cell: function cell(row) { return <Balance balance={row.value} /> }
  }
}
const volume = {
  Header: 'Volume',
  columns: [
    helper('1', '24H'),
    helper('7',  '7D'),
    helper('30', '30D'),
    {
      Header: 'Promiscuity',
      accessor: 'promiscuity',
      minWidth: 60
    },
  ]
}

export default volume
