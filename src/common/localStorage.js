function loader(section, initial) {
  try {
    const state = localStorage.getItem(section)
    if (state === null) {
      return undefined
    }
    return {
      [section]: {
        ...initial,
        ...JSON.parse(state)
      }
    }
  } catch (err) {
    return undefined
  }
}

export function loadState() {
  return {
    ...loader('contracts', require('../reducers/contracts').initial),
    ...loader('graph', require('../reducers/graph').initial)
  }
}

export function saveState(store) {
  try {
    const state = store.getState()
    localStorage.setItem('contracts', JSON.stringify({
      columns: state.contracts.columns,
      filter: state.contracts.filter
    }))
    localStorage.setItem('graph', JSON.stringify({
      core: state.graph.core,
      collections: state.graph.collections
    }))
  } catch (err) {
    console.error('error persisting state', err)
  }
}

