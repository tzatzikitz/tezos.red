import React from 'react'
const { number } = require('prop-types')

const Balance = ({ balance }) => {
  const str = (balance / 1000000).toLocaleString()
  const [a, b] = str.split('.')
  return (<span>{a}
    {b && <span style={{ opacity: 0.3 }}>.{b}</span>}&nbsp;ꜩ</span>)
}
Balance.propTypes = {
  balance: number
}

export default Balance
