import React, { Component } from 'react'
import ReactDOM from 'react-dom'
const { string } = require('prop-types')
const blocky = require('ethereum-blockies')


class Blockies extends Component {
  componentWillReceiveProps(props) {
    const node = ReactDOM.findDOMNode(this)
    blocky.render({
      seed: props.account,
      size: 8,
      scale: 8,
      spotcolor: '#000'
    }, node)
  }

  render() {
    return <canvas style={{ zIndex: 1, position: 'absolute', right: 0, marginRight: 30 }}></canvas>
  }

  static propTypes = {
    account: string
  }
}

export default Blockies
