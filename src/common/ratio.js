import React from 'react'
const { number } = require('prop-types')

const Ratio = ({ ratio }) => {
  if (!ratio) return null
  const str = ratio.toLocaleString()
  const [a, b] = str.split('.')
  return (<span>{a}
    {b && <span style={{ opacity: 0.3 }}>.{b}</span>}</span>)
}
Ratio.propTypes = {
  ratio: number
}

export default Ratio
