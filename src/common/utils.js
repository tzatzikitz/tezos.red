function getCode(script, prim = 'code') {
  let target = script && script.code && script.code.find(x => x.prim === prim)
  return target && target.args[0]
}

const groupBy = (xs, key) => {
  return xs.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || []).push(x)
    return rv
  }, {})
}

const get = (src, path, value) => {
  if (src == null) return value
  if (!path || !path.length) return src

  let parts = Array.isArray(path) ? path : path.split('.')
  let obj = src
  let i, ii

  for (i = 0, ii = parts.length; i < ii; ++i) {
    if (!obj.propertyIsEnumerable(parts[i])) {
      return value
    }

    obj = obj[parts[i]]

    if (obj == null) {
      return (i !== ii - 1) ? value : obj
    }
  }

  return obj
}

const pick = (src, props = [], into = {}, expand = false) => {
  return (src == null) ?
    into :
    props.reduce((res, key) => {
      const value = src[key]

      if (expand || typeof value !== 'undefined' || src.hasOwnProperty(key)) {
        res[key] = value
      }

      return res

    }, into)
}

const flatten = (obj, remove=[]) => {
  const res = {}

  function reduce(cur, prop = '') {
    if(remove.includes(cur)) return res
    if (Object(cur) !== cur) res[prop] = cur
    else for (let p in cur) reduce(cur[p], prop ? `${prop}.${p}` : p)

    return res
  }

  return reduce(obj)
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function uniq(array, into = [], memo = new Set()) {
  for (let item of array) {
    if (!memo.has(item)) {
      memo.add(item)
      into.push(item)
    }
  }
  return into
}

module.exports = {
  get,
  getCode,
  groupBy,
  pick,
  flatten,
  arraysEqual,
  uniq
}
