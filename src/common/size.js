const { keys } = Object
const PRIMS = new Set(['prim', 'args'])

function setDifference (a, b) {
  return new Set([...a].filter(x => !b.has(x)))
}

function walk(obj, handlerFn = () => {}) {
  if(!obj) return 0
  if(Array.isArray(obj))
    return obj.reduce((acc, curr) => acc + walk(curr, handlerFn), 0)

  // hit the handlerFn if obj has other fields besides 'prim' and 'args'
  const fields = new Set(keys(obj))
  if([...setDifference(fields, PRIMS)].length) handlerFn(obj)

  if(obj.prim) return 1 + walk(obj.args, handlerFn)
  return 0
}

function size(code) {
  return walk(code)
}

module.exports = {
  walk,
  size
}
