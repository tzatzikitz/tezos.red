module.exports = {
  RECEIVED: 'graph.received',
  TOGGLEPANEL: 'graph.togglepanel',
  CORE: {
    ADD: 'graph.core.add',
    REMOVE: 'graph.core.remove'
  },
  OPTIONS: {
    TOGGLELAYOUT: 'graph.options.toggleLayout'
  },
  COLLECTION: {
    LOAD: 'graph.collection.load',
    SAVE: 'graph.collection.save',
    REMOVE: 'graph.collection.remove'
  }
}
