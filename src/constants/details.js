module.exports = {
  FETCH: 'details.fetch',
  RECEIVED: {
    DETAILS: 'details.received',
    TRANSACTIONS: 'details.received.transactions'
  }
}
