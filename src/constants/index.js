module.exports = {
  SIMILARITY: require('./similarity'),
  META: require('./meta'),
  DETAILS: require('./details'),
  CONTRACTS: require('./contracts'),
  MANAGERS: require('./managers'),
  GRAPH: require('./graph'),
  DELEGATES: require('./delegates')
}
