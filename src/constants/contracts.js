module.exports = {
  TYPE: {
    ALL: 'all',
    SMART: 'smart',
    DUMB: 'dumb'
  },
  RECEIVED: 'contracts.received',
  TURNPAGE: 'contracts.turnPage',
  FILTER: {
    TOGGLE: 'contracts.filter.toggle',
    RESET: 'contracts.filter.reset',
    APPLY: 'contracts.filter.apply'
  },
  COLUMN: {
    TOGGLE: 'contracts.column.toggle'
  }
}
