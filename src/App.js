import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css'
import ContractList from './contracts/ContractList'
import Details from './details'
import Managers from './managers/Table'
import Delegates from './delegates/Table'
import Navbar from './Navbar'
import About from './About'
import Home from './Home'
import GraphFullscreen from './graph/fullscreen'
import Map from './map/Map'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar/>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/contracts/:xs/:xe/:ys/:ye' component={ContractList}/>
            <Route path='/contracts/manager/:manager' component={ContractList}/>
            <Route path='/contracts' component={ContractList}/>
            <Route path='/managers' component={Managers}/>
            <Route path='/delegates' component={Delegates}/>
            <Route path='/graph/:core' component={GraphFullscreen}/>
            <Route path='/graph' component={GraphFullscreen}/>
            <Route path='/about' component={About}/>
            <Route path='/map' component={Map}/>
            <Route path='/:contract' component={Details}/>
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
