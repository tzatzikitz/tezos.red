import React, { Component } from 'react'
import { any, string } from 'prop-types'
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import SimilarityModal from './similarity/Modal'
import './Navbar.css'

const NavLink = ({ to, children, re }) => {
  const pathname = window.location.pathname
  const active = re ? !!pathname.match(re) : (pathname === to)
  return (<NavItem componentClass={Link} href={to} to={to} active={active}>
    {children}
  </NavItem>)
}
NavLink.propTypes = {
  to: string.isRequired,
  children: any,
  re: any
}

export default class N extends Component {
  constructor() {
    super()
    this.state = {
      showSim: false
    }
  }

  render() {
    const { showSim } = this.state
    return (
      <Navbar fixedTop collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">ꜩ.red</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavLink to="/contracts" re={/^\/(KT|contracts)/i}>Contracts</NavLink>
          </Nav>
          <Nav>
            <NavLink to="/managers" re={/^\/(tz|manager)/}>Managers</NavLink>
          </Nav>
          <Nav>
            <NavItem active={showSim} onClick={() => this.setState({ showSim: true })}>
              Similarity
            </NavItem>
          </Nav>
          <Nav>
            <NavLink to="/delegates" re={/^\/delegates/}>Delegates</NavLink>
          </Nav>
          <Nav>
            <NavLink to="/graph" re={/^\/graph/}>Graph</NavLink>
          </Nav>
          <Nav>
            <NavLink to="/map" re={/^\/map/}>Map</NavLink>
          </Nav>
          <Nav pullRight>
            <NavLink to="/about">About</NavLink>
          </Nav>
          <Navbar.Text className="witticism" pullRight>
            <i>{'We\'re'} going <span className="red">rouge</span>!</i>
          </Navbar.Text>
        </Navbar.Collapse>
        <SimilarityModal show={showSim} onHide={() => this.setState({ showSim: false })} />
      </Navbar>
    )
  }
}
